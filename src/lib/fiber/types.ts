import { Sprite } from "spritejs";
import { IControl } from "../../store/modules/editor";

export enum EffectType {
    APPEND = 'append',
    UPDATE = 'update'
}

export interface INodeValue extends IControl{
    id: string;
};

export interface IEffectNode {
    type: EffectType,
    node: INodeValue
}