import { EffectType } from "./types";
import Button from '../../components/Render/Button';
import ImgComponent from '../../components/Render/ImgComponent';
import Video from '../../components/Render/Video';
import Audio from '../../components/Render/Audio';

export class LinkNode<T> {
    value: T;
    elemnt: Button| ImgComponent | Video | Audio;
    next: LinkNode<T> | null;
    constructor(value: T, element: Button| ImgComponent | Video | Audio, next: LinkNode<T> | null) {
        this.value = value;
        this.next = next;
        this.elemnt = element;
    }
}

export class EffectNode<T> {
    value: T;
    type: EffectType;
    next: EffectNode<T> | null;
    constructor(type: EffectType, value: T, next: EffectNode<T>| null) {
        this.type = type;
        this.value = value;
        this.next = next;
    }
} 

export class LinkList<T> {
    head: T;
    last: T;
    constructor(headNode: T, lastNode: T) {
        this.head = headNode;
        this.last = lastNode;
    }
}