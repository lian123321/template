export const removeElement = (id: string): void => {
    const htmlElement = document.getElementById(id);
    if(htmlElement) {
        document.body.removeChild(htmlElement);
    }
}