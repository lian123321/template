import { Watcher } from './../watcher/watcher';
import { Layer } from 'spritejs';

import { Sprite } from 'spritejs';
import Button from '../../components/Render/Button';
import ImgComponent from '../../components/Render/ImgComponent';
import Video from '../../components/Render/Video';
import Audio from '../../components/Render/Audio';
import { IStage, IControl } from './../../store/modules/editor/types';
import { LinkNode, EffectNode, LinkList } from './linkList';
import { INodeValue, EffectType } from './types';
import { commit } from './commit';
import _ from 'lodash';
import { Dep, getDep } from '../watcher';
import { Text } from '../../components/Render/Text';
export const typeMapToCreateFn = {
    button: (config: INodeValue): Button => {
      return new Button(config);
    },
    img:  (config: INodeValue): ImgComponent => {
      return new ImgComponent(config);
    },
    video: (config: INodeValue): Video  => {
      return new Video(config);
    },
    audio:  (config: INodeValue): Audio  => {
      return new Audio(config);
    },
    text: (config: INodeValue): Text => {
      return new Text(config);
    }
  };


let workInProgressRoot: LinkNode<INodeValue> | null = null;

let effectRoot: EffectNode<LinkNode<INodeValue>> | null = null;

let delNodes: Array<LinkNode<INodeValue>> = [];

let linkList: LinkList<LinkNode<INodeValue>>|null = null;

const dep = getDep();

export const schedule = (layer: Layer, stages: IStage[], controls: Record<string, IControl>, activeIndex: number, preActiveIndex: number): void => {
        /**
         * 如果切换了场景 则不在复用 
         * @todo 这里之后优化 可以复用对象
         */
        if(activeIndex !== preActiveIndex) {
            let currentNode =workInProgressRoot;
            while(currentNode) {
              delNodes.push(currentNode);
              currentNode = currentNode.next;
            }
            dep.clear();
            workInProgressRoot = null;
        }

        const currentStage = stages[activeIndex];

        const content = currentStage?.content || [];
        // 初次渲染
        if(!workInProgressRoot) {
            let preNode: LinkNode<INodeValue> | null = workInProgressRoot;
            let preEffect = effectRoot;
            // 生成链表
            content.forEach(controlId => {
                const id = Number(controlId);
                const controlOfControlId = controls[id];

                const config = {...controlOfControlId, id: controlId};
                const processFn  = (typeMapToCreateFn as any)[controlOfControlId.type];

                if(!processFn) return;
                const element = processFn(config);

                const node = new LinkNode<INodeValue>(config , element,  null);
                      
                const watcher = new Watcher(controlId, (...reset: any[]) => {
                  (node.elemnt as any)?.start?.(...reset);
                })

                dep.push(watcher);

                if(!preNode) {
                     workInProgressRoot = preNode = node;
                     preEffect = effectRoot = new EffectNode<LinkNode<INodeValue>>(EffectType.APPEND, node, null);
                     linkList =  new LinkList<LinkNode<INodeValue>>(workInProgressRoot,workInProgressRoot)
                     return;
                } 
                preNode.next = node;
                preNode = node;
          

                if(linkList) {
                   linkList.last = node;
                }

                if(preEffect) {
                const effectNode = new EffectNode<LinkNode<INodeValue>>(EffectType.APPEND, node, null);

                preEffect.next = effectNode;
                preEffect = effectNode;
             }
            });
        } else {
               /**
                * 非首次渲染 暂时先diff config
                */
                let currentNode: LinkNode<INodeValue> | null = workInProgressRoot;
                let preEffect:  EffectNode<LinkNode<INodeValue>> | null = effectRoot;
                const controlContents = stages[activeIndex]?.content;
                const stageContentLen = controlContents.length;

                let index = 0;
                let preNode: LinkNode<INodeValue>|null = currentNode;
                while((currentNode || index < stageContentLen ) && linkList) {
                    if(currentNode && index < stageContentLen) {
                        // 更新
                        const controlId = controlContents[index];
                 
                        // 如果当前链表结点的id对应不上content的id 则为删除
                        if(controlId !== currentNode.value.id) {
                            delNodes.push(currentNode);
                            // 删除链表头部
                            if(currentNode === workInProgressRoot) {
                               preNode = currentNode = workInProgressRoot = currentNode.next;
                            } else if(preNode) {
                               currentNode = preNode.next = currentNode.next;
                            }
                            continue;
                        }

                        const newConfig = {...controls[controlId], id: Number(controlId)};
                        const oldConfig = currentNode.value;
             
                        const isEqual = _.isEqual(newConfig, oldConfig);
                   
                        /// 更新配置
                        if(!isEqual) {
                            currentNode.value = {...controls[controlId], id: controlId};
                            const effectNode = new EffectNode(EffectType.UPDATE, currentNode, null);
                            if(!preEffect) {
                                preEffect = effectRoot = effectNode
                            } else {
                                preEffect.next = effectNode;
                            }
                            preEffect = effectNode;
                        }

                    }

                    if(currentNode && index >= stageContentLen) {
                        delNodes.push(currentNode);
                        // 删除最后一个结点
                        if(currentNode === workInProgressRoot && !workInProgressRoot?.next) {
                            workInProgressRoot = currentNode = null;
                            break;
                        }

                        if(preNode && currentNode) {
                            currentNode = preNode.next = currentNode.next;
                        }
                    }

                    if(!currentNode && index < stageContentLen) {
                        // 添加
                        const controlId = controlContents[index];
                        

                       const id = Number(controlId);

                       const controlOfControlId = controls[id];
       
                       const config = {...controlOfControlId, id: controlId};
                       const processFn  = (typeMapToCreateFn as any)[controlOfControlId.type];
       
                       if(!processFn) return;
       
                       const element = processFn(config);
       
                       const node = new LinkNode<INodeValue>(config , element,  null);

                       const lastNode = linkList.last;
                       lastNode.next = node;
                       linkList.last = node;
                       
                                  
                       const watcher = new Watcher(controlId, (...reset: any[]) => {
                         (node.elemnt as any)?.start?.(...reset);
                       })

                      dep.push(watcher);

                       const effectNode = new EffectNode(EffectType.APPEND, node, null);
                       if(!preEffect) {
                         preEffect = effectRoot = effectNode
                        } else {
                          preEffect.next = effectNode;
                         }
                        preEffect = effectNode;

                    }


                    index++;
                    currentNode = currentNode?.next || null;
                    preNode = currentNode;
                }

        }
        commit(layer, effectRoot, delNodes);
        delNodes = [];
        effectRoot = null;
        
};