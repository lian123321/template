import { Layer } from "spritejs";
import { getDep } from "../watcher";
import { EffectNode, LinkNode } from "./linkList";
import { EffectType, INodeValue } from "./types";
import { removeElement } from "./util";

const dep = getDep();

export const commit = (layer: Layer, effectRoot: EffectNode<LinkNode<INodeValue>> | null, delNodes:LinkNode<INodeValue>[] ): void => {
   
    delNodes.forEach(node => {     
        const element = node.elemnt;
        const nodeType = element.config.type;
        const id = node.value.id;

        if((element as any)?.componenWillUnmount) {
            (element as any)?.componenWillUnmount?.();
        } 
        if(!['audio'].includes(nodeType)){ 
           layer.removeChild(node.elemnt);
        } 
        
    })

    if(layer && effectRoot) {
     

        let currentNode: EffectNode<LinkNode<INodeValue>> | null = effectRoot;
        while(currentNode) {
            const { type, value: linkNode} = currentNode;
         
            const element = linkNode.elemnt;
            
            if(type === EffectType.APPEND) {
                if(!(element.config.type === 'audio')) {
                    layer.appendChild(element)
                }
            }
            if(type === EffectType.UPDATE) {
                element.update(linkNode.value)
            }
            
            currentNode = currentNode.next;
        }
    }
}