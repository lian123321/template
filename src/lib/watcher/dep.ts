import { Watcher } from "./watcher";

export class Dep {
    private watchers: Array<Watcher> = [];
    private state = false;
    constructor() {
        this.watchers = [];
    }

    push(watcher: Watcher): void {
        this.watchers.push(watcher);
    }

    del(id: string): void {
        this.watchers = this.watchers.filter(watch => watch.id !== id);
    }

    clear(): void {
        this.updateState(false);
        this.watchers = [];
    }
    updateState(state: boolean): void {
        if(this.state !== state) {
            this.state = state;
            this.update();
        }
    }

    update(): void {
        this.watchers.forEach(item => {
            item.update(this.state);
        })
    }
}

const dep = new Dep();

export const getDep = (): Dep => {
    return dep;
}