export class Watcher {
    id: string;
    update: (state: boolean) => void;
    constructor(id: string, callback: (state: boolean) => void) {
        this.id = id;
        this.update = callback;
    }
}
