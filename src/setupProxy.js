const { createProxyMiddleware } = require("http-proxy-middleware");

module.exports = function (app) {
  app.use(
    "/media/template",
    createProxyMiddleware({
      target: "http://localhost:8800",
      changeOrigin: true,
    })
  );
};
