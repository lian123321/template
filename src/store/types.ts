import { TEditorReducer } from './modules/editor/types';
import {  TCommonReducer } from './modules/common/types';
import { TReducerAction as TCommonReducerAction } from "./modules/common";
import { StateFromReducersMapObject } from 'redux';

export type TRootAction = TCommonReducerAction;


export type TRootCombineReducersType = {
    common: TCommonReducer;
    editor: TEditorReducer
}

export type TRootState = StateFromReducersMapObject<TRootCombineReducersType>;

