import { TRootCombineReducersType } from './types';
import { combineReducers, createStore, Middleware, applyMiddleware } from 'redux';
import { commonReducer } from './modules/common';
import reduxThunk from 'redux-thunk';
import edtiorReducer from './modules/editor/reducer';

const middlewares: Middleware[] = [reduxThunk];

const reducer = combineReducers<TRootCombineReducersType>({
    common: commonReducer,
    editor: edtiorReducer
});

const createInstance = () => {
    //  暂时不需要redux调试工具 所以暂时不注入钩子
    const store = createStore(reducer,applyMiddleware(...middlewares));
    return store;
}

export default createInstance();