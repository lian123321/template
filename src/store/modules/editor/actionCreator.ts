import { CHANGE_ACTIVE_IMG, EControlEventType, IAudioPlayEvent, IProject, IToggleStageEvent, TAddEventOfCurrentControl, TChangeCurrentStageName, TClearEventOfCurrentControl, TDelStageAction, TEvent, TInitProject } from '.';
import { CHANGE_ACTIVE_CONTROL, CHANGE_ACTIVE_INDEX, Add_ACTIVE_CONTROL_OF_ACTIVE, Add_STAGE, DEL_STAGE, CHANGE_SELECT_CONTROL, DELETE_SELECTED_CONTROL_ID, TOGGLE_PLAY_STATE, CHANGE_CURRENT_TIME, CHANGE_PROJECT_NAME, INIT_PROJECT, CHANGE_CURRENT_STAGE_NAME, ADD_EVENT_OF_CUTTENT_CONTROL, CLEAR_EVENT_OF_CUTTENT_CONTROL } from './actionTypes';
import { IControl, IControlActionData, TAddActiveControlAction, TAddStageAction, TChangeActiveAction, TChangeActiveControlAction, TChangeActiveImg, TChangeCurrentTime, TChangeProjectName, TChangeSelectControlAction, TDelSelectedControlOfId, TTogglePlayStateAction } from "./types"

export const getChangeActiveIndexAction = (index: number):TChangeActiveAction  => {
    return {
        type: CHANGE_ACTIVE_INDEX,
        data: String(index)
    }
}



export const getChangeActiveControlAction = (data: IControlActionData):TChangeActiveControlAction  => {
    return {
        type: CHANGE_ACTIVE_CONTROL,
        data
    }
}

export const getAddControlOfActiveAction = (data: IControl):TAddActiveControlAction  => {
    return {
        type: Add_ACTIVE_CONTROL_OF_ACTIVE,
        data
    }
}

export const getAddStageAction = ():TAddStageAction  => {
    return {
        type: Add_STAGE,
        data: null
    }
}

export const getDelStageAction = (id: string):TDelStageAction  => {
    return {
        type: DEL_STAGE,
        data: id
    }
}


export const getChangeSelectControlAction = (id: string):TChangeSelectControlAction  => {
    return {
        type: CHANGE_SELECT_CONTROL,
        data: id
    }
}

export const getDelSelectedControlOfId = ():TDelSelectedControlOfId  => {
    return {
        type: DELETE_SELECTED_CONTROL_ID,
        data: null
    }
}

export const getTogglePlayStateAction = ():TTogglePlayStateAction  => {
    return {
        type: TOGGLE_PLAY_STATE,
        data: null
    }
}

export const getChangeCurrentTimeAction = (time: number):TChangeCurrentTime  => {
    return {
        type: CHANGE_CURRENT_TIME,
        data: String(time)
    }
}

export const getChangeActiveImg = (index: string,img: string):TChangeActiveImg  => {
    return {
        type: CHANGE_ACTIVE_IMG,
        data: {
            index,
            img
        }
    }
}
// TChangeProjectName
export const getChangeProjectName = (name: string):TChangeProjectName  => {
    return {
        type: CHANGE_PROJECT_NAME,
        data: name
    }
}

export const getInitProject = (project: IProject):TInitProject  => {
    return {
        type: INIT_PROJECT,
        data: project
    }
}

export const getChangeCurrentStageName = (name: string):TChangeCurrentStageName  => {
    return {
        type: CHANGE_CURRENT_STAGE_NAME,
        data: name
    }
}

export const getAddEventOfCurrentControl = (data: TEvent):TAddEventOfCurrentControl  => {
    return {
        type: ADD_EVENT_OF_CUTTENT_CONTROL,
        data: data
    }
}

export const getClearEventOfCurrentControl = (data: EControlEventType):TClearEventOfCurrentControl  => {
    return {
        type: CLEAR_EVENT_OF_CUTTENT_CONTROL,
        data: data
    }
}