import { Add_ACTIVE_CONTROL_OF_ACTIVE, CHANGE_ACTIVE_CONTROL, CHANGE_ACTIVE_INDEX, Add_STAGE, DEL_STAGE, CHANGE_SELECT_CONTROL, DELETE_SELECTED_CONTROL_ID, TOGGLE_PLAY_STATE, CHANGE_CURRENT_TIME, CHANGE_ACTIVE_IMG, CHANGE_PROJECT_NAME, CHANGE_CURRENT_STAGE_NAME, ADD_EVENT_OF_CUTTENT_CONTROL, CLEAR_EVENT_OF_CUTTENT_CONTROL } from './actionTypes';
import { Action, Reducer } from 'redux';
import { INIT_PROJECT } from '.';
export interface IAction<T,D> extends Action<T> {
    data: D;
}


export interface IStage {
    id: string;
    img: string;
    name: string;
    content: Array<string>;
}

export enum EControlEventType {
    Click = 'click',
    AudioPlay = 'audioPlay'
}

export interface ICommonEvent {
    delay ?: number; // 如果不传delay 则为 0
}
export interface IToggleStageEvent extends ICommonEvent {
    area: string;
}

export interface IAudioPlayEvent extends ICommonEvent{
    url: string;
}

export type TEvent = {
    [EControlEventType.Click] ?: IToggleStageEvent,
    [EControlEventType.AudioPlay] ?: IAudioPlayEvent
};

export interface IControl {
    type: string;
    text: string;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    style: any;
    box: {
        pos: Array<number>;
        size: Array<number>;
    },
    data?:{
        url?:string;
    },
    name: string;
    event ?: TEvent;
}
export interface ITemplateData {
    controls: Record<string, IControl>
    stages: Array<IStage>;
}


export interface IControlActionData {
    id: string;
    data: IControl
}


export interface IControlActionData {
    id: string;
    data: IControl
}

export interface IProject {
    id: string;
    name: string;
    template: ITemplateData;
}

export type TChangeActiveAction  =  IAction<typeof CHANGE_ACTIVE_INDEX, string>;
export type TChangeActiveControlAction  =  IAction<typeof CHANGE_ACTIVE_CONTROL, IControlActionData>;

export type TAddStageAction  =  IAction<typeof Add_STAGE, null>;
export type TDelStageAction  =  IAction<typeof DEL_STAGE, string>;

export type TAddActiveControlAction  =  IAction<typeof Add_ACTIVE_CONTROL_OF_ACTIVE, IControl>;

export type TChangeSelectControlAction  =  IAction<typeof CHANGE_SELECT_CONTROL, string>;

export type TDelSelectedControlOfId  =  IAction<typeof DELETE_SELECTED_CONTROL_ID, null>;

export type TTogglePlayStateAction  =  IAction<typeof TOGGLE_PLAY_STATE, null>;

export type TChangeCurrentTime  =  IAction<typeof CHANGE_CURRENT_TIME, string>;

export type TChangeProjectName  =  IAction<typeof CHANGE_PROJECT_NAME, string>;

export type TChangeActiveImg  =  IAction<typeof CHANGE_ACTIVE_IMG, {index: string, img: string}>;

export type TInitProject = IAction<typeof INIT_PROJECT, IProject>;

export type TChangeCurrentStageName = IAction<typeof CHANGE_CURRENT_STAGE_NAME, string>;

export type TAddEventOfCurrentControl = IAction<typeof ADD_EVENT_OF_CUTTENT_CONTROL, TEvent>;

export type TClearEventOfCurrentControl = IAction<typeof CLEAR_EVENT_OF_CUTTENT_CONTROL, EControlEventType>;

export type TReducerAction = TClearEventOfCurrentControl | TAddEventOfCurrentControl | TChangeCurrentStageName | TInitProject | TChangeProjectName| TChangeActiveImg | TChangeActiveAction | TChangeActiveControlAction | TAddActiveControlAction | TAddStageAction | TDelStageAction | TChangeSelectControlAction | TDelSelectedControlOfId | TTogglePlayStateAction | TChangeCurrentTime;

export interface IState {
    project: IProject;
    activeIndex: string;
    selectControlId: string;
    start: boolean;
    currentTime: string;
}

export type TEditorReducer = Reducer<IState,TReducerAction>;