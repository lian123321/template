import { Action } from 'redux';
import { CHANGE_ACTIVE_CONTROL, IState, TDelStageAction } from '.';

import { CHANGE_ACTIVE_INDEX, Add_ACTIVE_CONTROL_OF_ACTIVE, Add_STAGE, DEL_STAGE, CHANGE_SELECT_CONTROL, DELETE_SELECTED_CONTROL_ID, TOGGLE_PLAY_STATE, CHANGE_CURRENT_TIME, CHANGE_ACTIVE_IMG, CHANGE_PROJECT_NAME, INIT_PROJECT, CHANGE_CURRENT_STAGE_NAME, ADD_EVENT_OF_CUTTENT_CONTROL, CLEAR_EVENT_OF_CUTTENT_CONTROL } from './actionTypes';
import { TChangeActiveAction, TEditorReducer, TChangeActiveControlAction, TAddActiveControlAction, TAddStageAction, TTogglePlayStateAction, TChangeCurrentTime, TChangeActiveImg, TChangeProjectName, TInitProject, TChangeCurrentStageName, TAddEventOfCurrentControl, TClearEventOfCurrentControl } from './types';
import _, { template } from 'lodash';

const editorDefaultState = {
    project: {
      id: '12',
      name: '新建模板',
      template: {
        stages: [
            {
                id: '1',
                img: 'https://lf3-creativelab-sign.bytetos.com/creative-lab/material/20bb6edf6a910a94eda2d903d6975483.jpg?x-expires=1640409436&x-signature=XN62EVDF3B09UBprOplmj6bNkoA%3D',
                content: [],
                name: '主场景'
            },
            // {
            //     id: '12',
            //     img: 'https://lf3-creativelab-sign.bytetos.com/creative-lab/material/20bb6edf6a910a94eda2d903d6975483.jpg?x-expires=1640409436&x-signature=XN62EVDF3B09UBprOplmj6bNkoA%3D',
            //     content: ["101","105"],
            //     name: '主场景'
            // }
        ],
        controls: {
            // "100": {
            //     type: "button",
            //     text: "张三",
            //     style: {
            //     },
            //     box: {
            //         pos :[1200, 700],
            //         size :[476, 99]
            //     }
            // },
            // "101": {
            //     type: "button",
            //     text: "张三",
            //     style: {
            //     },
            //     box: {
            //         pos :[900, 400],
            //         size :[476, 120]
            //     },
            //     data: {
            //         url: 'http://localhost:3000/button.png'
            //     }
            // },
            // "102": {
            //     type:"img",
            //     text:"",
            //     style:{},
            //     data:{
            //         url:'https://lf3-creativelab-sign.bytetos.com/creative-lab/material/20bb6edf6a910a94eda2d903d6975483.jpg?x-expires=1640409436&x-signature=XN62EVDF3B09UBprOplmj6bNkoA%3D'
            //     },
            //     box: {
            //         pos :[0, 0],
            //         size :[375, 812]
            //     }
            // },
            // "103": {
            //     type:"video",
            //     text:"",
            //     style:{},
            //     data:{
            //         url:'http://localhost:3000/video.mp4'
            //     },
            //     box: {
            //         pos :[0, 0],
            //         size :[400, 400]
            //     }
            // },
            // "104": {
            //     type:"audio",
            //     text:"",
            //     style:{},
            //     data:{
            //         url:'https://m801.music.126.net/20220108110829/3a20997b0ab896a2500ba6b8b1dc8bb2/jdymusic/obj/w5zDlMODwrDDiGjCn8Ky/2023378805/a54c/c304/f4e4/73f00b48c06ae209ae8f7e5e9dba2f9b.mp3'
            //     },
            //     box: {
            //         pos :[0, 0],
            //         size :[0, 0]
            //     }
            // },
            // "105": {
            //     type:"audio",
            //     text:"",
            //     style:{},
            //     data:{
            //         url:'https://m7.music.126.net/20220103173734/15426c50db4566003750e6133e435137/ymusic/obj/w5zDlMODwrDDiGjCn8Ky/3572085975/025a/aa52/b742/1d4e1b4214d707d02543a20d47c353c8.mp3'
            //     },
            //     box: {
            //         pos :[0, 0],
            //         size :[0, 0]
            //     }
            // }
        }

    },
    },
    activeIndex: '0',
    selectControlId:'-1',
    currentTime: '0',
    start: false
}


const typeMapToProcessFn = {
    [CHANGE_ACTIVE_INDEX](state: IState, action: TChangeActiveAction): IState {
        const { data } = action;
        return {
            ...state,
            selectControlId: '-1',
            activeIndex: data,
            start: false,
            currentTime: '0',
        }
    },
    [CHANGE_ACTIVE_CONTROL](state: IState, action: TChangeActiveControlAction): IState {
        const { data: { id, data } } = action;
        const newState = _.clone(state);
        newState.project.template.controls[id] = data;
        return newState;
    },
    [Add_ACTIVE_CONTROL_OF_ACTIVE](state: IState, action: TAddActiveControlAction): IState {
        const { data } = action;
        const newState = _.clone(state);
        const id = parseInt(String(Math.random() * 100001));
        const { activeIndex, project: { template } } = newState;
        template.stages[Number(activeIndex)].content.push(String(id));
        template.controls[id] = data;
        return {
            ...newState
        }
    },
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    [Add_STAGE](state: IState, action: TAddStageAction): IState {
        const id = parseInt(String(Math.random() * 100001));
        const defaultStage =  {
            id: String(id),
            img: 'https://lf3-creativelab-sign.bytetos.com/creative-lab/material/20bb6edf6a910a94eda2d903d6975483.jpg?x-expires=1640409436&x-signature=XN62EVDF3B09UBprOplmj6bNkoA%3D',
            content: [],
            name: "空白场景"
        };

        const newState = _.clone(state);
        newState.project.template.stages.push(defaultStage);
        return newState;
    },
    [DEL_STAGE](state: IState, action: TDelStageAction): IState {
      
      const id = action.data;
      const newState = _.clone(state);
      
      const { project: { template }, activeIndex } = newState;
      const { stages } = template;
      template.stages = stages.filter(item => item.id !== id);

      return newState;
    },
    [CHANGE_SELECT_CONTROL](state: IState, action: TDelStageAction): IState {
        const controlId = action.data;
        const newState = _.clone(state);
        newState.selectControlId = String(controlId);

        return newState;
      },

      [DELETE_SELECTED_CONTROL_ID](state: IState, action: TDelStageAction): IState {

        const newState = _.clone(state);

        const { selectControlId, activeIndex, project: { template } } = newState;

        const  { stages, controls } = template;

        const currentStage = stages[Number(activeIndex)];
        const newControl = _.omit(controls, [selectControlId]);

        currentStage.content = currentStage.content.filter(id => id !== selectControlId);
        template.controls = newControl;
        
        newState.selectControlId = '-1';

        return newState;
      },
      [TOGGLE_PLAY_STATE](state: IState, action: TTogglePlayStateAction) {
    
        const newState = _.clone(state);
        newState.start = !newState.start;
        return newState;
      },
      [CHANGE_CURRENT_TIME](state: IState, action: TChangeCurrentTime) {
        const newState = _.clone(state);
        newState.currentTime = action.data;
        return newState;
      },
      [CHANGE_ACTIVE_IMG](state: IState, action: TChangeActiveImg) {
        const newState = _.clone(state);
        const { index, img } = action.data;
        const stages = newState.project.template.stages;
        if(stages[Number(index)]) {
            stages[Number(index)].img = img
        }
        return newState;
      },
      [CHANGE_PROJECT_NAME](state: IState, action: TChangeProjectName) {
          const newState = _.cloneDeep(state);
          const newProjectName = action.data;

          newState.project.name = newProjectName;

          return newState;
      },
      [INIT_PROJECT](state: IState, action: TInitProject) {
          const newState = _.clone(state);
       
          const { data  } = action;
          
          newState.project = data;

          return newState;
      },
      [CHANGE_CURRENT_STAGE_NAME](state: IState, action: TChangeCurrentStageName) {
          const newState = _.clone(state);
          const currentActiveIndex = newState.activeIndex;
          const stage = newState.project.template.stages[Number(currentActiveIndex)];
          stage.name = action.data || '';
          return newState;
      },
      [ADD_EVENT_OF_CUTTENT_CONTROL](state: IState, action: TAddEventOfCurrentControl) {
          
          const newState = _.cloneDeep(state);

          const activeControlId = newState.selectControlId;

          const currentControl = newState.project.template.controls[activeControlId];
          if(currentControl) {

            const data = action.data;
            let event = currentControl?.event || {};
            // eslint-disable-next-line @typescript-eslint/no-unused-vars
            event = Object.assign(event, data);

            currentControl.event = event;
          }

          return newState;
      },
      [CLEAR_EVENT_OF_CUTTENT_CONTROL](state: IState, action: TClearEventOfCurrentControl) {
          
        const newState = _.cloneDeep(state);

        const activeControlId = newState.selectControlId;

        const currentControl = newState.project.template.controls[activeControlId];

        const eventKey = action.data;

        if(currentControl) {
            currentControl.event = _.omit(currentControl.event, eventKey);
        }

        return newState;
    },
};
const edtiorReducer: TEditorReducer = (state = editorDefaultState, action) => {
    const { type } = action;
    const processFn = typeMapToProcessFn[type];
    if(processFn) {
        return processFn(state, action as any);
    }
    return state;
}

export default edtiorReducer;