import { Action, Reducer } from 'redux';
import {IChangeEditorAction} from './actionCreator';
export interface IAction<T,D> extends Action<T> {
    data: D;
}

export type TReducerAction = IChangeEditorAction;

export interface IState {
 
}

export type TCommonReducer = Reducer<IState,TReducerAction>;