import { IChangeEditorAction } from './actionCreator';
import { IState, TCommonReducer } from './types';
import { CHANGE_EDITOR } from './actionTypes';

const defaultState: IState = {
} 

const mapTypeToProcessFn = {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    [CHANGE_EDITOR](state: IState, action: IChangeEditorAction) {
        return state;
    }
}
//DEMON
// eslint-disable-next-line @typescript-eslint/no-unused-vars
export const commonReducer: TCommonReducer = (state = defaultState, action) => {
    const type = action.type;
    const processFn = (mapTypeToProcessFn as any)[type];
    if(processFn) {
        return (mapTypeToProcessFn as any)(state, action);
    }
    return state;
}

