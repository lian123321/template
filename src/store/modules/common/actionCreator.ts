import { IAction } from './types';
import { CHANGE_EDITOR } from './actionTypes';
// DEMON
export type IChangeEditorAction = IAction<typeof CHANGE_EDITOR, string>;
export const getChangeEditorAction = (data: string): IChangeEditorAction => {
    return {
        type:CHANGE_EDITOR,
        data
    };
}

