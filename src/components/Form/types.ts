import { Rule } from "antd/lib/form";
import { EFormType } from "./constant";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export interface ICreateItemProps<T = any> {
    type: EFormType;
    formItemProps:{
        name ?: string;
        style ?: React.CSSProperties;
        rule ?: Rule[];
        valuePropName ?: string;
    },
    componentProps: {
        prefix ?: React.ReactNode,
        value ?: T;
        onChange ?: (val: T) => void;
        width ?: string;
    },
    childrenOptions?: Array<{ label: string, value: T }>
}