import { Form, Input, InputNumber, Select } from "antd";
import React from "react";
// 表单工具

import { EFormType } from "./constant";
import { ICreateItemProps } from "./types";

const SelectOption = Select.Option;

const formTypeMapToElement = {
  [EFormType.Input](componentProps: ICreateItemProps["componentProps"]) {
    return <Input {...componentProps} />;
  },
  [EFormType.Selecet](
    componentProps: ICreateItemProps["componentProps"],
    childrenOptions: ICreateItemProps["childrenOptions"]
  ) {
    return (
      <Select {...componentProps}>
        {childrenOptions?.map((item) => {
          return (
            <SelectOption key={item.label} value={item.value}>
              {item.label}
            </SelectOption>
          );
        })}
      </Select>
    );
  },
  [EFormType.InputNumber](componentProps: ICreateItemProps["componentProps"]) {
    return <InputNumber {...componentProps} />;
  },
};
const createItem = <T,>(props: ICreateItemProps<T>): JSX.Element => {
  const { type, formItemProps, componentProps, childrenOptions } = props;

  const processFn = formTypeMapToElement[type];
  let formElement: JSX.Element = <div></div>;
  if (processFn) {
    formElement = processFn(componentProps, childrenOptions);
  }

  return <Form.Item {...formItemProps}>{formElement}</Form.Item>;
};
export { createItem };
