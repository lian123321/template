import { Sprite } from "spritejs";
import Box from "../Box";
import TemplateContainer from "../TemplateContainer";
import { captureVideo } from "./util";

class Video extends Box {
  config: any;
  element: Sprite;
  videoElement?: HTMLVideoElement;
  constructor(props: any) {
    super({ ...props, zIndex: 2 });
    this.config = props;
    const element = new Sprite();
    this.element = element;
    this.appendChild(element);
    this.initVideo();
    this.render();
    this.tick();
  }
  initVideo(): void {
    const { id, data } = this.config;
    let videoElement = document.getElementById(id) as HTMLVideoElement;
    if (!videoElement) {
      videoElement = document.createElement("video");
      videoElement.src = data?.url;
      videoElement.id = id;
      videoElement.muted = false;
      videoElement.preload = "auto";
      videoElement.width = 0;
      videoElement.height = 0;
      document.body.appendChild(videoElement);
      // videoElement.play();
    }

    this.videoElement = videoElement;
  }
  private render() {
    const {
      box: { size, pos },
      data,
    } = this.config;
    const { element, videoElement } = this;

    if (videoElement) {
      // videoElement.src = data?.url;
      videoElement.oncanplay = () => {
        element.attr({
          size,
          pos,
          texture: captureVideo(videoElement),
        });
      };
    }
  }
  update(config: any): void {
    this.config = config;
    const {
      box: { size, pos },
      data,
    } = this.config;
    const { element, videoElement } = this;
    if (videoElement) {
      element.attr({
        size,
        pos,
        texture: captureVideo(videoElement),
      });
    }
  }

  start(playState: boolean): void {
    const { videoElement } = this;
    if (playState) {
      videoElement?.play();
    } else {
      videoElement?.pause();
    }
  }

  updateElement(): void {
    const {
      box: { size, pos },
    } = this.config;
    const { element, videoElement } = this;
    if (videoElement) {
      element.attr({
        texture: captureVideo(videoElement),
      });
    }
  }
  setCurrentTime(): void {
    const { videoElement } = this;
    if (videoElement && videoElement.paused) {
      videoElement.currentTime = TemplateContainer.currentTime;
    }
  }
  tick(): void {
    window.requestAnimationFrame(() => {
      this.setCurrentTime();
      this.updateElement();
      this.tick();
    });
  }
}

export default Video;
