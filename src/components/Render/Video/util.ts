// 获取视频帧
const canvasId = 'canvas-capture-img';
export const captureVideo = (video: HTMLVideoElement): string => {
    let canvas = document.getElementById(canvasId) as HTMLCanvasElement | null;
    
    const videoWidth = 300;
    const videoHeight = 300;

    if(!canvas) {
        canvas = document.createElement('canvas');
        canvas.id = canvasId;
        canvas.style.position = 'fixed';
        canvas.style.top = '-10000px';
        canvas.width = 300;
        canvas.height = 300;
        document.body.appendChild(canvas);
    };
    
    const context = canvas.getContext('2d');
    context?.drawImage(video,0,0,videoWidth,videoHeight)
    const imgData = context?.getImageData(0,0,videoWidth,videoHeight);

    return imgData as unknown as string;
}