import { Sprite } from "spritejs";
import Box from "../Box";

class ImgComponent extends Box {
  config: any;
  element: Sprite;
  constructor(props: any) {
    super({ ...props, zIndex: 1 });
    this.config = props;
    const img = new Sprite();
    this.element = img;
    this.appendChild(img);
    this.render();
  }
  setAnimate(): void {
    if (this.config.style.animationName) {
      this.registerAnimate(this.element, this.config);
    }
  }
  private render(): void {
    const { box, data } = this.config;
    const element = this.element;
    const { pos, size } = box;
    element.attr({
      pos,
      size,
      texture: data?.url,
      zIndex: 0,
    });
    this.setAnimate();
  }

  update(config: any): void {
    this.config = config;
    this.render();
  }
}

export default ImgComponent;
