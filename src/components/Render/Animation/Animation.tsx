import spritejs, { Block, Group } from "spritejs";
import { IStyleAnimate } from "../../../pages/Editor/components/EditorProperty/components/Animation";
import { IControl } from "../../../store/modules/editor";
import { EAnimationType } from "../Button/types";

export class Animation extends Group {
  private cAnimate: spritejs.Animation | null;
  constructor(props: any) {
    super(props);
    this.cAnimate = null;
  }
  registerAnimate(element: Block, config: IControl): void {
    if (this.cAnimate) {
      this.cAnimate.cancel();
      this.cAnimate = null;
    }
    if (
      config.style.animationName === EAnimationType.Normal ||
      !config.style.animationName
    ) {
      return;
    }
    const style = config.style as IStyleAnimate;
    const {
      animationName,
      animationDuration,
      animationEndValue,
      animationStartValue,
      animationIterations,
      animationDirection,
    } = style;

    this.cAnimate = element.animate(
      [
        {
          [animationName]: Number(animationStartValue) || 1,
        },
        { [animationName]: Number(animationEndValue) || 1 },
      ],
      {
        duration: Number(animationDuration),
        iterations: animationIterations,
        direction: animationDirection,
      }
    );
  }
}
