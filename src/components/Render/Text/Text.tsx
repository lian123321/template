import { Label, Sprite } from "spritejs";
import Box from "../Box";
import TemplateContainer, {
  createTemplateInstance,
} from "../TemplateContainer";
import $style from "./style.module.less";

export class Text extends Box {
  config: any;
  element: Label;
  constructor(props: any) {
    super({ ...props, zIndex: 2 });
    this.config = props;
    const text = new Label();

    // this.addEventListener("click", (e) => {
    //   const canvas = document.querySelector('[data-layer-id="editor"]');

    //   const { left: cL, top: cT } = canvas?.getBoundingClientRect() as any;
    //   const container = createTemplateInstance();

    //   const textPos = text.getAttribute("pos");

    //   const textSize = text.getAttribute("size");

    //   const layerScale = container.layerScale;

    //   const input = document.createElement("textArea") as HTMLTextAreaElement;
    //   input.style.lineHeight = "22px";
    //   input.style.position = "fixed";
    //   input.style.top = container.y + textPos[1] + cT + "px";
    //   input.style.left = container.x + textPos[0] + cL + "px";

    //   input.style.transformOrigin = "center";
    //   input.style.transform = `scale(${layerScale})`;
    //   input.style.width = textSize[0] + "px";
    //   input.style.height = textSize[1] + "px";

    //   input.style.fontFamily = this.config.style.fontFamily;
    //   input.style.fontSize = this.config.style.fontSize + "px";

    //   input.style.textAlign = this.config.style.textAlign;
    //   input.style.color = this.config.style.fontColor;

    //   input.value = this.config.style.textContent;

    //   input.autofocus = true;
    //   input.className = "textInputTextArea";

    //   this.removeChild(this.element);
    //   input.addEventListener("change", (e: Event) => {
    //     const inputText = (e.target as any).value;
    //     const id = (this.config as any).id;
    //     this.config.style.textContent = inputText;
    //     container.changeCurrentControl(id, this.config);
    //   });

    //   input.addEventListener("blur", () => {
    //     document.body.removeChild(input);
    //     this.appendChild(this.element);
    //     this.update(this.config);
    //   });
    //   document.body.appendChild(input);
    //   input.focus();
    // });

    this.element = text;
    this.appendChild(text);
    this.render();
  }

  setAnimate(): void {
    if (this.config.style.animationName) {
      this.registerAnimate(this.element, this.config);
    }
  }

  private render(): void {
    const { box, data, text, style } = this.config;

    const { textContent, fontColor, fontSize, textAlign, verticalAlign } =
      style;

    const element = this.element;
    const { pos, size } = box;
    element.attr({
      pos,
      width: size[0],
      // size,
      fillColor: fontColor,
      fontSize,
      textAlign,
      verticalAlign,
      lineHeight: 22,
    });
    element.text = textContent;
    this.setAnimate();
  }
  update(config: any): void {
    this.config = config;
    this.render();
  }
}
