import { size } from "lodash";
import { Layer, Scene, Sprite } from "spritejs";
import { IControl, ITemplateData } from "../../../store/modules/editor";
import Button from "../Button";
import { rootContainerId } from "../constant";
import ImgComponent from "../ImgComponent";
import Video from "../Video";
import Audio from "../Audio";
import { EEffectType } from "../types";
import { schedule } from "../../../lib/fiber/schedule";
import { getDep } from "../../../lib/watcher";
import html2canvas from "html2canvas";

class TemplateContainerEvent {
  eventMap: Record<string, (...reset: any[]) => void> = {
    changeControl: () => {},
    selectControl: () => {},
    changeCurrentControl: () => {},
  };
  listen(type: string, callback: (...reset: any[]) => void) {
    this.eventMap[type] = callback;
    return this;
  }
  changeControl(id: string, size: Array<number>, pos: Array<number>) {
    this.eventMap?.changeControl(id, size, pos);
    return this;
  }
  selectControl(id: string) {
    this.eventMap?.selectControl(id);
    return this;
  }
  changeCurrentControl(id: string, config: IControl) {
    this.eventMap?.changeCurrentControl(id, config);
    return this;
  }
}

/**
 * 1.单例模式 createInstance
 * 2. 挂载 mount
 * 3. render 渲染子组件
 */
export class TemplateContainer extends TemplateContainerEvent {
  private scene?: Scene;
  private layer?: Layer;
  private scale = 1;
  private container?: HTMLDivElement;
  private isEnter = false;
  public x = 0;
  public y = 0;
  private template?: ITemplateData;
  private activeIndex = String(0);
  private static sprite?: Sprite;
  public layerScale = 1;
  public static currentTime = 0;
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  constructor() {
    super();
  }

  isNoSameActive(activeIndex: string): boolean {
    return !Object.is(this.activeIndex, activeIndex);
  }

  getActiveIndex(): string {
    return this.activeIndex;
  }
  mount(container: HTMLDivElement): TemplateContainer {
    const scene = new Scene({
      container,
      autoRender: false,
    });
    scene.attr({});
    const layer = scene.layer("editor");
    this.scene = scene;
    this.layer = layer;
    this.container = container;
    this.addEvent(scene, layer, container);

    return this;
  }
  render(template: ITemplateData, activeIndex: string): void {
    this.template = template;

    const preActiveIndex = this.activeIndex;

    this.activeIndex = activeIndex;
    // 处理数据结构 映射到组件类 暂时不支持二级 三级子组件
    const { stages, controls } = template;

    const { scale, layer, container, scene } = this;
    if (!(layer && container && scene)) return;

    // 添加手机绘制背景
    const containerWidth = container.offsetWidth;
    const containerHeight = container.offsetHeight;

    if (containerHeight < 900) {
      this.layerScale = containerHeight / 900;
    }
    const boxX = (containerWidth - 375) / 2;
    const boxY = (containerHeight - 812) / 2;
    this.x = boxX;
    this.y = boxY;

    const box = new Sprite({
      size: [375, 812],
      pos: [0, 0],
      bgcolor: "#000",
      id: rootContainerId,
    });

    layer.attr({
      transformOrigin: [containerWidth / 2, containerHeight / 2],
      scale: this.layerScale,
      pos: [boxX, boxY],
      size: [375, 812],
    });

    schedule(
      layer,
      stages,
      controls,
      Number(activeIndex),
      Number(preActiveIndex)
    );

    layer.removeChild(box);
    layer.appendChild(box);

    /**关于fiber 链表 */
    layer.render();
  }

  private addEvent(
    scene: Scene,
    layer: Layer,
    container: HTMLDivElement
  ): void {
    document.addEventListener("mousewheel", (e) => {
      if (!this.isEnter) {
        return;
      }

      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const isAhead = (e as any).deltaY < 0;
      if (isAhead) {
        if (this.scale > 1) {
          return;
        }
        this.scale += 0.01;
      } else {
        if (this.scale < 0.4) {
          return;
        }
        this.scale -= 0.01;
      }
      const canvas = document.querySelector(
        '[data-layer-id="editor"]'
      ) as HTMLCanvasElement;

      if (canvas) {
        canvas.style.transformOrigin = "center,center";
        canvas.style.transform = `scale(${this.scale})`;
      }
      // layer.render();
    });

    window.addEventListener("resize", () => {
      scene.render();
    });

    container.addEventListener("mouseenter", () => {
      this.isEnter = true;
    });
    container.addEventListener("mouseleave", () => {
      this.isEnter = false;
    });
  }

  // 16.6ms重新绘制一次
  autoPaint(): void {
    const { template } = this;
    window.requestAnimationFrame(() => {
      this.autoPaint();
    });
    if (template) {
      this.layer?.render();
    }
  }

  public captureImg(): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      const scene = this.scene;

      if (!scene) {
        return reject("");
      }
      const currentCanvas = scene.snapshot() as HTMLCanvasElement;
      const pos = this.layer?.getAttribute("pos");

      const size = this.layer?.getAttribute("size");
      const imgData = currentCanvas.toDataURL("image/png");
      const img = document.createElement("img");
      img.src = imgData;

      const layerScale = this.layerScale;

      img.onload = () => {
        const canvas = document.createElement("canvas");
        canvas.width = img.width;
        canvas.height = img.height;
        const ctx = canvas.getContext("2d");
        if (ctx) {
          // ctx?.translate(0, 0);
          // ctx.fillStyle = "red";
          // ctx?.fillRect(0, 0, img.width, img.height);
          ctx.beginPath();
          ctx?.drawImage(img, 0, 0);
          ctx.closePath();
        }

        const imgCanvas = document.createElement("canvas");
        imgCanvas.width = size[0] * layerScale;
        imgCanvas.height = size[1] * layerScale;

        const imgCtx = imgCanvas.getContext("2d");

        const data = ctx?.getImageData(
          pos[0] + (375 - 375 * layerScale) / 2,
          pos[1] + (812 - 812 * layerScale) / 2,
          size[0] * layerScale,
          size[1] * layerScale
        );
        if (data && imgCtx) {
          imgCtx?.putImageData(data, 0, 0);
        }
        resolve(imgCanvas.toDataURL());
      };
    });
  }
}
const template = new TemplateContainer();
export const createTemplateInstance = (): TemplateContainer => {
  return template;
};

export default TemplateContainer;
