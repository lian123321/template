import { IControl } from "../../../store/modules/editor";
import TemplateContainer from "../TemplateContainer";

class Audio {
  config: any;
  element: HTMLAudioElement;
  constructor(props: any) {
    this.config = props;
    const id = props.id;
    let aduio = document.getElementById(id) as HTMLAudioElement;
    if (!aduio) {
      aduio = document.createElement("audio");
      aduio.id = id;
      aduio.autoplay = false;
      aduio.preload = "auto";
      document.body.appendChild(aduio);
    }
    this.element = aduio;
    this.render();
  }
  private render(): void {
    const { data } = this.config;
    const element = this.element;
    // element.src = data?.url;
    if (!element.src) {
      element.src = data?.url;
    }
  }

  start(playState: boolean): void {
    const { element } = this;

    if (playState) {
      // element.play();
    } else {
      element.pause();
    }
  }

  setCurrentTime(): void {
    const { element } = this;
    if (element && element.paused) {
      element.currentTime = TemplateContainer.currentTime;
    }
  }

  update(config: any): void {
    if (this.config.data.url !== config.data.url) {
      this.element.src = config.data.url;
      this.element.load();
      // this.element.play();
    }
    this.config = config;

    this.render();
    this.setCurrentTime();
  }
  componenWillUnmount(): void {
    const element = this.element;
    if (!element.paused) {
      element.pause();
    }
    if (element.parentNode) {
      const parentNode = element.parentNode;
      parentNode.removeChild(element);
    }
  }
}

export default Audio;
