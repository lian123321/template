import { useForm } from "antd/lib/form/Form";
import { group } from "console";
import spritejs, { Group, Layer, Rect, Sprite } from "spritejs";
import { rootContainerId } from "../constant";
import { createTemplateInstance } from "../TemplateContainer";
import { Animation } from "../Animation";
class Box extends Animation {
  mouseDown = false;
  rects: Sprite[] = [];
  position: Array<number> = [];
  size: Array<number> = [];
  OffsetX = 0;
  OffsetY = 0;
  box?: Sprite;
  elementId = "";
  selected = false;
  constructor(props: any) {
    super({ ...props });
    const { box, id } = props;
    const { pos, size } = box;
    this.elementId = id;
    this.mouseMove = this.mouseMove.bind(this);
    this.addEvent();

    // this.renderSelectBox(pos, size);
    this.position = pos;
    this.size = size;
  }

  getPosition(): Array<number> {
    return this.position;
  }

  addEvent(): void {
    const { box } = this;

    const intance = createTemplateInstance();
    this.addEventListener("mouseenter", (e) => {
      if (!this.mouseDown) {
        box?.attr({
          border: [2, "#6380ff"],
        });
        document.body.style.cursor = "pointer";
      } else {
        document.body.style.cursor = "move";
      }
      e.stopPropagation();
    });

    this.addEventListener("mouseleave", (e) => {
      if (!this.mouseDown) {
        box?.attr({
          border: [0, "#6380ff"],
        });
      }
      document.body.style.cursor = "";
      // document.body.style.cursor = "";
      this.mouseDown = false;
      e.stopPropagation();
    });

    this.addEventListener("mousedown", (e) => {
      const { position, id } = this;
      this.children[0]?.attr({
        border: [1, "#6380ff"],
      });
      document.body.style.cursor = "move";
      this.mouseDown = true;

      this.OffsetX = position[0] - e.layerX;
      this.OffsetY = position[1] - e.layerY;

      e.stopPropagation();
    });

    this.addEventListener("click", (e) => {
      const { id } = this;
      intance.selectControl(id);
      this.selected = true;
      e.stopPropagation();
    });
    this.addEventListener("mousemove", this.mouseMove);

    this.addEventListener("mouseup", (e) => {
      this.mouseDown = false;
      const { position, size, elementId } = this;

      this.children[0]?.attr({
        border: [0, "#6380ff"],
      });

      intance.changeControl(elementId, position, size);

      // this.selected = false;
      document.body.style.cursor = "";
      e.stopPropagation();
    });
  }
  mouseMove(e: spritejs.Event): void {
    if (this.mouseDown) {
      const { OffsetX, OffsetY } = this;
      const x = e.x + OffsetX;
      const y = e.y + OffsetY;
      this.children.forEach((node) => {
        node.attr({
          pos: [x, y],
        });
      });
      this.position = [x, y];
    }
    // this.renderSelectBox(this.position as any, this.size as any);
    e.stopPropagation();
  }
}
export default Box;
