import { Group, Layer, Sprite } from "spritejs";
import { IControl } from "../../../store/modules/editor";
import Box from "../Box";

export interface IButtonProps {
  layer: Layer;
  box: IControl["box"];
}

class Button extends Box {
  config: any;
  element: Sprite;
  constructor(props: any) {
    super({ ...props, zIndex: 3 });
    this.config = props;
    const { box, data } = props;
    const { pos, size } = box;
    const button = new Sprite();
    this.element = button;

    this.render();

    this.appendChild(button);
  }

  private render() {
    const config = this.config;
    const { box, data } = config;
    const { pos, size } = box;
    const element = this.element;
    element.attr({
      pos,
      size,
      zIndex: 100,
      texture: data?.url,
      transformOrigin: [size[0] / 2, size[1] / 2],
    });

    this.setAnimate();
    // 缩放
    // element.animate([{ scale: 1 }, { scale: 1.2 }], {
    //   duration: 1000,
    //   iterations: Infinity,
    //   direction: "alternate",
    // });

    // 淡入
    // element.animate([{ opacity: 0 }, { opacity: 1 }], {
    //   duration: 2000,
    //   iterations: 1,
    //   direction: "alternate",
    // });

    // 旋转
    // element.animate([{ rotate: 0 }, { rotate: 360 }], {
    //   duration: 3000,
    //   iterations: Infinity,
    // });
  }

  setAnimate(): void {
    if (this.config.style.animationName) {
      this.registerAnimate(this.element, this.config);
    }
  }
  update(config: any): void {
    this.config = config;
    this.render();
  }
}

export default Button;
