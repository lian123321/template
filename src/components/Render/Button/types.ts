export enum EAnimationType {
    Scale = 'scale',
    Opacity = 'opacity',
    Rotate = 'rotate',
    Normal = 'normal'
}

export enum EDirection {
    Normal = 'normal',
    Alternate = 'alternate'
}