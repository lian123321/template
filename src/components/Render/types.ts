export enum EEffectType {
    ADD = 'ADD',
    DEL = 'DEL',
    MODIFY = 'MODIFY'
}