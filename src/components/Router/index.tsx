import React from "react";
import { useGetRoutes } from "../../route";
import { BrowserRouter, Routes, Route } from "react-router-dom";
// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface IProps {}

const Router: React.FC<IProps> = () => {
  const routes = useGetRoutes();

  return (
    <>
      <BrowserRouter>
        <Routes>
          {routes.map((route) => {
            const { path, key, show, component: Component } = route;
            if (!show) {
              return null;
            }
            return <Route path={path} key={key} element={<Component />} />;
          })}
        </Routes>
      </BrowserRouter>
    </>
  );
};

export default Router;
