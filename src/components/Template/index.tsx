import React, { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getDep } from "../../lib/watcher";
import {
  getChangeActiveControlAction,
  getChangeActiveImg,
  getChangeSelectControlAction,
  IControl,
  IState,
  ITemplateData,
} from "../../store/modules/editor";
import { TRootState } from "../../store/types";
import TemplateContainer, {
  createTemplateInstance,
} from "../Render/TemplateContainer";
import $style from "./style.module.less";

const dep = getDep();
// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface IProps {}

const Template: React.FC<IProps> = () => {
  const dispatch = useDispatch();

  const templateRef = useRef<HTMLDivElement>(null);
  const editor = useSelector<TRootState, IState>((state) => state.editor);
  const {
    activeIndex,
    project: { template },
    start,
    currentTime,
  } = editor;

  const [templateInstance, setTemplateInstance] = useState<TemplateContainer>();

  const dataRef = useRef<ITemplateData>();
  dataRef.current = template;

  const handleChangeControl = (
    id: string,
    pos: Array<number>,
    size: Array<number>
  ) => {
    const template = dataRef.current;
    if (!template) {
      return;
    }
    const control = template.controls[id];
    const { box } = control;
    dispatch(
      getChangeActiveControlAction({
        id,
        data: { ...control, box: { ...box, size, pos } },
      })
    );
  };

  const handleChangeSelectControl = (id: string) => {
    dispatch(getChangeSelectControlAction(id));
  };

  const handleChangeCurrentControl = (id: string, config: IControl) => {
    dispatch(
      getChangeActiveControlAction({
        id,
        data: config,
      })
    );
  };
  TemplateContainer.currentTime = Number(currentTime || 0);
  // INIT
  useEffect(() => {
    const container = templateRef.current;
    if (container) {
      const templateInstance = createTemplateInstance();

      templateInstance.mount(container).autoPaint();
      setTemplateInstance(templateInstance);

      dep.updateState(start);
      templateInstance
        .listen("changeControl", handleChangeControl)
        .listen("selectControl", handleChangeSelectControl)
        .listen("changeCurrentControl", handleChangeCurrentControl);
    }
  }, []);

  useEffect(() => {
    if (templateInstance) {
      dep.updateState(start);

      templateInstance.render(template, activeIndex);
    }
  }, [editor, activeIndex, templateInstance]);

  return (
    <>
      <div className={$style.templateWrapper} ref={templateRef}></div>
    </>
  );
};

export default Template;
