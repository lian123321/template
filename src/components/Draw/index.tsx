import { Spin } from "antd";
import React from "react";

import $style from "./style.module.less";

import { Left, Right } from "@icon-park/react";
import classNames from "classnames";
import { noop } from "lodash";
interface IProps {
  loading: boolean;
  content: JSX.Element;
  show: boolean;
  onClose: () => void;
  onOpen: () => void;
}

const Draw: React.FC<IProps> = (props) => {
  // eslint-disable-next-line react/prop-types
  const { loading, content, show, onClose = noop, onOpen = noop } = props;

  const handleToggle = () => {
    if (show) {
      onClose();
      return;
    }
    onOpen();
  };
  return (
    <div className={$style.drawWrapper}>
      <div className={classNames($style.close)} onClick={handleToggle}>
        {show ? (
          <Left size="20" fill="#3c3c3c" theme="outline" />
        ) : (
          <Right size="20" fill="#3c3c3c" theme="outline" />
        )}
      </div>
      <div
        className={classNames(
          $style.drawSpinWrapper,
          show ? $style.drawOpen : $style.drawClose
        )}
      >
        <div className={$style.contentWrapper}>
          <Spin
            spinning={loading}
            className={classNames($style.DrawContentLoadding)}
          >
            {content}
          </Spin>
        </div>
      </div>
    </div>
  );
};

export default Draw;
