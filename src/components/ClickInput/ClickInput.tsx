import { Input } from "antd";
import React, { useEffect, useState } from "react";

interface IProps {
  value: string;
  handleChange: (value: string) => void;
}

export const ClickInput: React.FC<IProps> = ({ value, handleChange }) => {
  const [showInput, setShowInput] = useState(false);

  const [inputValue, setInputValue] = useState(value);
  const toggleState = () => {
    setShowInput(!showInput);
  };

  const handleInput = (e: any) => {
    const value = e.target.value;
    setInputValue(value);
  };

  const handleBlur = () => {
    handleChange(inputValue);
    toggleState();
  };
  useEffect(() => {
    setInputValue(value);
  }, [value]);

  if (showInput) {
    return (
      <div>
        <Input value={inputValue} onInput={handleInput} onBlur={handleBlur} />
      </div>
    );
  }
  return <div onClick={toggleState}>{value}</div>;
};
