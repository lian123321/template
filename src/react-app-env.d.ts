/// <reference types="react-scripts" />

declare module "*.less" {
    const less: any;
    export default less;
  }

  declare module "*.png" {
    const content: any;
    export default content;
  }

  declare interface GlobalThis {
    isOnline: boolean;
  }

  declare const isOnline: boolean;