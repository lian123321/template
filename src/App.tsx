import React from "react";
import { Provider } from "react-redux";
import Router from "./components/Router";
import store from "./store";
import $style from "./app.module.less";
// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface Props {}

const App: React.FC<Props> = () => {
  return (
    <Provider store={store}>
      <div className={$style.appWrapper}>
        <Router />
      </div>
    </Provider>
  );
};

export default App;
