import React, { useState } from "react";
import $style from "./style.module.less";
import { Picture, Video, Radio, Components, Text } from "@icon-park/react";
import { Spin } from "antd";
import Draw from "../../../../../../components/Draw";
import { useDispatch } from "react-redux";
import {
  Add_ACTIVE_CONTROL_OF_ACTIVE,
  getAddControlOfActiveAction,
} from "../../../../../../store/modules/editor";
import classNames from "classnames";
import { getUrl } from "../../../../../../utils";

interface IProps {}

const ToolBar: React.FC<IProps> = (props) => {
  const dispatch = useDispatch();

  const [openDraw, setOpenDraw] = useState(false);

  const [activeIndex, setActiveIndex] = useState(-1);
  const handleOpenDraw = () => {
    setOpenDraw(true);
  };

  const handleCloseDraw = () => {
    setOpenDraw(false);
  };

  const renderImgContent = () => {
    return (
      <div className={$style.drawContent}>
        {imgListConfig.map((item, i) => {
          const { name, url } = item;
          return (
            <div
              key={i}
              className={$style.drawContentItemWrapper}
              onClick={() => handleAddControlOfActive(i, imgListConfig, "img")}
            >
              <div className={$style.drawImg}>
                <img src={url} />
              </div>
              <div className={$style.drawText}>{name}</div>
            </div>
          );
        })}
      </div>
    );
  };

  // 渲染音频
  const renderAudioAndVideoContent = (type: string) => {
    const list = type === "audio" ? audioListConfig : videoListConfig;
    return (
      <div className={classNames($style.drawContent, $style.videoListWrapper)}>
        {list.map((item, i) => {
          return (
            <div
              className={$style.audioItem}
              key={item.name}
              onClick={() => handleAddControlOfActive(i, list, type)}
            >
              <div className={$style.audioImg}>
                <img src={item.img} />
              </div>
              <div className={$style.desc}>
                <div className={$style.descName}>
                  <span>{item.name}</span>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    );
  };

  // 渲染组件

  const renderComponentList = () => {
    return (
      <div className={classNames($style.drawContent, $style.componentWrapper)}>
        {componentList.map((item, i) => {
          return (
            <div
              key={i}
              onClick={() => handleAddComponent(i)}
              className={$style.componentItemWrapper}
            >
              <div className={$style.componentItemImg}>
                <img src={item.img} />
              </div>
              <div className={$style.componentItemText}>{item.name}</div>
            </div>
          );
        })}
      </div>
    );
  };

  // 文字

  const renderTextList = () => {
    const handleAddText = () => {
      dispatch(
        getAddControlOfActiveAction({
          name: "文本",
          type: "text",
          text: "普通文本",
          style: {
            textContent: "普通文本",
          },
          data: {
            url: "",
          },
          box: {
            pos: [0, 0],
            size: [200, 100],
          },
        })
      );
    };
    return (
      <div className={$style.textListWrapper}>
        <div className={$style.textP} onClick={handleAddText}>
          普通文本
        </div>
      </div>
    );
  };
  const handleAddComponent = (index: number) => {
    const control = componentList[index].dsl;
    dispatch(getAddControlOfActiveAction(control));
  };

  const handleAddControlOfActive = (
    i: number,
    configs: Array<any>,
    type: string
  ) => {
    const config = configs[i];
    dispatch(
      getAddControlOfActiveAction({
        name: config.name,
        type,
        text: "",
        style: {},
        data: {
          url: config.url,
        },
        box: {
          pos: [0, 0],
          size: [375, 812],
        },
      })
    );
  };

  const toolBarConfig = [
    {
      key: "图片",
      icon: Picture,
      content: renderImgContent,
    },
    {
      key: "视频",
      icon: Video,
      content: renderAudioAndVideoContent.bind(this, "video"),
    },
    {
      key: "音频",
      icon: Radio,
      content: renderAudioAndVideoContent.bind(this, "audio"),
    },
    {
      key: "组件",
      icon: Components,
      content: renderComponentList,
    },
    {
      key: "文字",
      icon: Text,
      content: renderTextList,
    },
  ];

  const imgListConfig = [
    {
      name: "远方",
      url: "https://lf3-creativelab-sign.bytetos.com/creative-lab/material/20bb6edf6a910a94eda2d903d6975483.jpg?x-expires=1640409436&x-signature=XN62EVDF3B09UBprOplmj6bNkoA%3D",
    },
    {
      name: "诗歌",
      url: "https://creativelab-proxy.bytedance.com/api/upload/bucket/material/a1a79552471cc14c9b6e4562be5be57c.jpg",
    },
    {
      name: "梦想",
      url: "https://creativelab-proxy.bytedance.com/api/upload/bucket/material/804f93a196e24ee10b08815ffa2ee641.jpg",
    },
  ];

  const audioListConfig = [
    {
      name: "问心",
      url: `${getUrl()}/static/wenxin.m4a`,
      img: "https://y.qq.com/music/photo_new/T002R300x300M000000Dki3X1sRzZ5_3.jpg",
    },
  ];

  const videoListConfig = [
    {
      name: "没用的阿吉（视频功能无法使用，暂时只支持本地）",
      url: "http://localhost:3000/video.mp4",
      img: "https://y.qq.com/music/photo_new/T002R300x300M000000Dki3X1sRzZ5_3.jpg",
    },
  ];

  const componentList = [
    {
      name: "再看一下",
      img: "https://lf9-creativelab-sign.bytetos.com/creative-lab/material/private/b8e505ced113056b8444804894931785.png?x-expires=1647175481&x-signature=Cf7iZvaDp4vM5egXj5tRPbSAN7c%3D",
      dsl: {
        type: "button",
        text: "再看一下",
        name: "再看一下",
        style: {},
        box: {
          pos: [0, 0],
          size: [100, 100],
        },
        data: {
          url: "https://lf9-creativelab-sign.bytetos.com/creative-lab/material/private/b8e505ced113056b8444804894931785.png?x-expires=1647175481&x-signature=Cf7iZvaDp4vM5egXj5tRPbSAN7c%3D",
        },
      },
    },
  ];
  return (
    <div className={$style.toolbarWrapper}>
      {toolBarConfig.map((item, i) => {
        const { key, icon: Icon } = item;
        return (
          <div
            key={key}
            className={classNames(
              activeIndex === i && $style.activeBarItem,
              $style.toolBarItem
            )}
            onClick={() => {
              handleOpenDraw();
              setActiveIndex(i);
            }}
          >
            <div className={$style.toolBarItemIcon}>
              <Icon size="20" fill="#333" theme="filled" />
            </div>
            <div className={$style.toolBarItemText}>{key}</div>
          </div>
        );
      })}
      <div className={$style.draw}>
        <Draw
          loading={false}
          content={toolBarConfig[activeIndex]?.content()}
          show={openDraw}
          onClose={handleCloseDraw}
          onOpen={handleOpenDraw}
        />
      </div>
    </div>
  );
};

export default ToolBar;
