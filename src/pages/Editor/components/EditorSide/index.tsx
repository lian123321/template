import React from "react";
import EditorStages from "../EditorStages";
import ToolBar from "./components/ToolBar";
import $style from "./style.module.less";

interface Props {}

const EditorSide: React.FC<Props> = (props) => {
  return (
    <div className={$style.editorSideWrapper}>
      <EditorStages />
      <ToolBar />
    </div>
  );
};

export default EditorSide;
