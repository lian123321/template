import { Button } from "antd";
import React from "react";
import { useDispatch } from "react-redux";
import { getAddStageAction } from "../../../../store/modules/editor";
import StageList from "./components/StageList";
import $style from "./style.module.less";

interface IProps {}

const EditorStages: React.FC<IProps> = (props) => {
  const dispatch = useDispatch();

  const handleCreateStage = () => {
    dispatch(getAddStageAction());
  };
  return (
    <div className={$style.stagesWrapper}>
      <div className={$style.createBtnWrapper}>
        <Button
          type="primary"
          className={$style.createBtn}
          onClick={handleCreateStage}
        >
          添加空白场景
        </Button>
      </div>
      <div className={$style.stageListWrapper}>
        <StageList />
      </div>
    </div>
  );
};

export default EditorStages;
