import React, { useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { createTemplateInstance } from "../../../../../../components/Render/TemplateContainer";
import {
  getChangeActiveImg,
  getChangeActiveIndexAction,
  IState,
} from "../../../../../../store/modules/editor";
import { TRootState } from "../../../../../../store/types";
import StageCard from "./components/StagesCard";
import $style from "./style.module.less";
// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface IProps {}

const StageList: React.FC<IProps> = () => {
  const dispatch = useDispatch();

  const editor = useSelector<TRootState, IState>((state) => state.editor);

  const {
    project: { template },
    activeIndex,
  } = editor;
  const stages = template.stages;

  const handleStageClick = useCallback(
    (index: number) => {
      const templateInstance = createTemplateInstance();
      // 判断是切换了场景 就截图
      const oldIndex = activeIndex;
      templateInstance.captureImg().then(
        (img) => {
          dispatch(getChangeActiveImg(oldIndex, img));
        },
        () => {}
      );

      dispatch(getChangeActiveIndexAction(index));
    },
    [activeIndex]
  );

  return (
    <div className={$style.stageListWrapper}>
      {stages.map((item, i) => {
        const isSelected = Number(activeIndex) === i;

        return (
          <div key={item.id} onClick={() => handleStageClick(i)}>
            <StageCard
              disbaled={stages.length === 1}
              stage={item}
              isSelected={isSelected}
            />
          </div>
        );
      })}
    </div>
  );
};

export default StageList;
