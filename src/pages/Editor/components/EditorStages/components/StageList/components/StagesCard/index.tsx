import React from "react";
import {
  getDelStageAction,
  IStage,
} from "../../../../../../../../store/modules/editor";
import $style from "./style.module.less";
import classnames from "classnames";
import { Delete } from "@icon-park/react";
import { useDispatch } from "react-redux";
import classNames from "classnames";

interface IProps {
  stage: IStage;
  isSelected: boolean;
  disbaled: boolean;
}

const StageCard: React.FC<IProps> = (props) => {
  // eslint-disable-next-line react/prop-types
  const dispatch = useDispatch();
  // eslint-disable-next-line react/prop-types
  const { isSelected, stage, disbaled } = props;
  // eslint-disable-next-line react/prop-types
  const { id, img, name } = stage;

  const operationConfig = [
    {
      icon: Delete,
      onClick: (id: string) => {
        dispatch(getDelStageAction(id));
      },
    },
  ];
  return (
    <div className={$style.stageWrapper}>
      <div
        className={classnames($style.stageCard, isSelected && $style.active)}
        style={{ backgroundImage: `url(${img})` }}
      >
        <div className={classNames(disbaled && $style.hidden, $style.toolBar)}>
          {operationConfig.map((item, i) => {
            const { icon: Icon, onClick } = item;
            return (
              <div
                key={i}
                className={$style.toolBarItem}
                onClick={(e) => {
                  onClick(id);
                  e.stopPropagation();
                }}
              >
                <Icon size="14" fill="#fff" theme="outline" />
              </div>
            );
          })}
        </div>
      </div>
      <div
        className={classNames(
          $style.stageText,
          isSelected && $style.activeText
        )}
      >
        {name}
      </div>
    </div>
  );
};

export default StageCard;
