import React, { useCallback, useRef, useState } from "react";
import $style from "./style.module.less";
import { useDispatch, useSelector } from "react-redux";
import { TRootState } from "../../../../store/types";
import { getChangeProjectName, IState } from "../../../../store/modules/editor";
import { Button, Input, message } from "antd";
import { saveTemplate } from "../../../../services/template";
import { getUrl, link } from "../../../../utils/apiUtil";
import EditorPreview from "../EditorPreview/EditorPreview";

interface IProps {}

const EditorHeader: React.FC<IProps> = (props) => {
  const dispatch = useDispatch();

  const editor = useSelector<TRootState, IState>((state) => state.editor);

  const [inputValue, setInputValue] = useState("");

  const { project } = editor;

  const [isEditProject, setIsEditProject] = useState(false);

  const [showPreviewBox, setShowPreviewBox] = useState(false);

  const handleClick = useCallback(() => {
    setInputValue(project.name);
    setIsEditProject(!isEditProject);
  }, [isEditProject, project]);

  const handleBlur = useCallback(
    (e) => {
      dispatch(getChangeProjectName(inputValue));
      setIsEditProject(!isEditProject);
    },
    [inputValue]
  );

  const handleInputValue = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      const val = e.target.value;
      setInputValue(val);
    },
    []
  );

  const handleSaveProject = useCallback(() => {
    saveTemplate(project)
      .then(() => {
        message.info("保存成功");
      })
      .catch((e) => {
        message.error(e);
      });
  }, [project]);

  const handleClickPreview = useCallback(() => {
    setShowPreviewBox(!showPreviewBox);
  }, [showPreviewBox]);

  return (
    <>
      <div className={$style.editorHeader}>
        <div className={$style.leftWrapper}>
          <div className={$style.logo}>
            <img src="https://img.zcool.cn/community/01766f58eb6c2ca8012049ef0313a5.jpg@1280w_1l_2o_100sh.jpg" />
          </div>
          <div className={$style.templateDesc}>
            {!isEditProject ? (
              <div onClick={handleClick}>{project.name}</div>
            ) : (
              <Input
                autoFocus={true}
                placeholder={project.name}
                onChange={handleInputValue}
                onBlur={handleBlur}
                value={inputValue}
              />
            )}
          </div>
        </div>
        <div className={$style.rightWrapper}>
          <Button
            type="primary"
            size="large"
            onClick={handleClickPreview}
            style={{ marginRight: "20px" }}
          >
            预览
          </Button>

          <Button type="primary" size="large" onClick={handleSaveProject}>
            保存
          </Button>
        </div>
      </div>
      {showPreviewBox && <EditorPreview onClose={handleClickPreview} />}
    </>
  );
};

export default EditorHeader;
