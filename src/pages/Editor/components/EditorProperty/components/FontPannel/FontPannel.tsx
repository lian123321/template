import React, { useEffect } from "react";

import { Form, Input, Radio, Select } from "antd";
import TextArea from "antd/lib/input/TextArea";

import $style from "./style.module.less";
import classNames from "classnames";
import { IControl } from "../../../../../../store/modules/editor";

const Option = Select.Option;

interface IProps {
  handleChange: (values: IFontStyle) => void;
  control: IControl;
}

export interface IFontStyle {
  textContent: string;
  fontFamily: string;
  fontSize: string;
  fontColor: string;
  textAlign: string;
  verticalAlign: string;
}
export const FontPannel: React.FC<IProps> = ({ control, handleChange }) => {
  const [form] = Form.useForm<IFontStyle>();

  useEffect(() => {
    const style = control.style as IFontStyle;
    form.setFieldsValue({ ...style });
  }, [control]);

  const verticalAlignOption = [
    { label: "默认", value: "normal" },
    { label: "顶端", value: "top" },
    { label: "居中", value: "middle" },
    { label: "低端", value: "bottom" },
  ];
  const textAlignOption = [
    { label: "默认", value: "normal" },
    { label: "左对齐", value: "left" },
    { label: "居中", value: "center" },
    { label: "右对齐", value: "right" },
  ];

  const fontStyleOption = [
    { label: "加粗", value: "bold" },
    { label: "斜体", value: "fontStyle" },
  ];

  const fontOptions = [
    { label: "默认", value: "auto" },
    { label: "Serif", value: "Serif" },
    { label: "Sans-serif", value: "Sans-serif" },
    { label: "Monospace", value: "Monospace" },
    { label: "Cursive", value: "Cursive" },
    { label: "Fantasy", value: "Fantasy" },
  ];

  const handleFormChange = (value: any, formValue: IFontStyle) => {
    handleChange(formValue);
  };
  return (
    <Form form={form} onValuesChange={handleFormChange}>
      <div className={$style.Wrapper}>
        <div className={$style.labelWrapper}>文本内容</div>
        <Form.Item name="textContent">
          <TextArea
            showCount
            maxLength={100}
            style={{ height: 120 }}
            value={""}
            onChange={() => {}}
          />
        </Form.Item>
      </div>

      <div className={$style.Wrapper}>
        <div className={$style.labelWrapper}>字体</div>
        <Form.Item name="fontFamily">
          <Select style={{ width: "100%" }}>
            {fontOptions.map((item) => {
              const { label, value } = item;

              return (
                <Option key={value} value={value}>
                  {label}
                </Option>
              );
            })}
          </Select>
        </Form.Item>
      </div>

      <div className={classNames($style.Wrapper, $style.flexWrapper)}>
        <div>
          <div className={$style.labelWrapper}>字体大小</div>
          <Form.Item name="fontSize">
            <Input placeholder="字体大小" />
          </Form.Item>
        </div>
        <div>
          <div className={$style.labelWrapper}>字体颜色</div>
          <Form.Item name="fontColor">
            <input type="color" />
          </Form.Item>
        </div>
      </div>

      <div className={$style.Wrapper}>
        <div className={$style.labelWrapper}>水平对齐方式</div>
        <div>
          <Form.Item name="textAlign">
            <Radio.Group options={textAlignOption} optionType="button" />
          </Form.Item>
        </div>
      </div>

      <div className={$style.Wrapper}>
        <div className={$style.labelWrapper}>垂直对齐方式</div>
        <div>
          <Form.Item name="verticalAlign">
            <Radio.Group options={verticalAlignOption} optionType="button" />
          </Form.Item>
        </div>
      </div>
    </Form>
  );
};
