import React from "react";
import { EFormType, ICreateItemProps } from "../../../../../../components/Form";

export const toggleStageConfigList: Array<ICreateItemProps> = [
  {
    type: EFormType.InputNumber,
    componentProps: {
      prefix: <div>延迟时间</div>,
    },
    formItemProps: {
      name: "delay",
    },
  },
  {
    type: EFormType.Selecet,
    componentProps: {},
    formItemProps: {
      name: "area",
    },
    childrenOptions: [],
  },
];

export const audioPlayConfigList: Array<ICreateItemProps> = [
  {
    type: EFormType.InputNumber,
    componentProps: {
      prefix: <div>延迟时间</div>,
    },
    formItemProps: {
      name: "delay",
    },
  },
];

import { EControlEventType } from "../../../../../../store/modules/editor";

export const eventList = [
  {
    label: "切换场景",
    value: EControlEventType.Click,
  },
  {
    label: "播放音效",
    value: EControlEventType.AudioPlay,
  },
];
