import { Close } from "@icon-park/react";
import React from "react";

import $style from "./style.module.less";
interface IProps {
  content?: JSX.Element;
  onClose: () => void;
  title: string;
}

export const EventDialog: React.FC<IProps> = ({ content, onClose, title }) => {
  return (
    <div className={$style.eventPropertDialog}>
      <div className={$style.dialogWrapper}>
        <div className={$style.dialogHd}>
          <span className={$style.dialogHdText}>{title}</span>
          <div className={$style.dialogClose}>
            <Close size="16" theme="filled" fill="#3c3c3c" onClick={onClose} />
          </div>
        </div>
        <div className={$style.main}>{content}</div>
      </div>
    </div>
  );
};
