import React, { useEffect, useRef, useState } from "react";
import $style from "./style.module.less";
import { Music, Play, PauseOne, Info } from "@icon-park/react";
import { message, Upload } from "antd";
import { getUrl } from "../../../../../../../../../../utils";
import { AudioState } from "./components/AudioState";
import {
  EControlEventType,
  getAddEventOfCurrentControl,
  IAudioPlayEvent,
} from "../../../../../../../../../../store/modules/editor";
import { useDispatch } from "react-redux";
import { formate } from "./util";

interface IProps {
  handleChange: (url: string) => void;
  data: string;
}

export const MusicUpload: React.FC<IProps> = ({ handleChange, data }) => {
  const uploadConfig = {
    name: "file",
    action: `${getUrl()}/media/template/infor`,
    headers: {},
    onChange(info: any) {
      const name = info.file.name;
      if (info.file.status !== "uploading") {
        console.log(info.file, info.fileList);
      }
      if (info.file.status === "done") {
        const url = getUrl() + "/static/" + name;
        handleChange(url);
        message.success(`${info.file.name} file uploaded successfully`);
      } else if (info.file.status === "error") {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
  };

  const [audioConfig, setAudioConfig] = useState({
    total: 0,
    current: 0,
  });
  const [playState, setPlayState] = useState(false);

  const audioRef = useRef<HTMLAudioElement>(null);

  const handleAudioClick = () => {
    const element = audioRef.current;
    if (element) {
      if (playState) {
        element.pause();
        setPlayState(false);
        return;
      }

      element.play();
      setPlayState(true);
    }
  };

  const handleAudioEnd = () => {
    setPlayState(false);
  };

  const handleAudioLoaded = () => {
    const element = audioRef.current;
    if (element) {
      setAudioConfig({ total: element.duration, current: 0 });
    }
  };

  const handlePlaying = () => {
    const element = audioRef.current;
    if (element) {
      setAudioConfig({ ...audioConfig, current: element.currentTime });
    }
  };

  return (
    <div className={$style.musicUploadWrapper}>
      <div className={$style.musicPlay}>
        {data ? (
          <div onClick={handleAudioClick}>
            <AudioState isPlay={playState} />
          </div>
        ) : (
          <Music size="30" theme="filled" fill="#ccc" />
        )}
      </div>
      <div className={$style.musicProperty}>
        <div className={$style.musicPropertyLabel}>
          {data?.split("/static/")[1]}
        </div>
        {/* <div>STEP</div> */}
      </div>
      <div className={$style.musicOpt}>
        <div>
          <Upload {...uploadConfig}>
            <span className={$style.uploadOpt}>上传</span>
          </Upload>
        </div>
        <div className={$style.time}>
          {formate(audioConfig.current, audioConfig.total)}
        </div>
      </div>
      <div className={$style.audioWrapper}>
        <audio
          ref={audioRef}
          onEnded={handleAudioEnd}
          src={data}
          preload="auto"
          onCanPlay={handleAudioLoaded}
          onTimeUpdate={handlePlaying}
        />
      </div>
    </div>
  );
};
