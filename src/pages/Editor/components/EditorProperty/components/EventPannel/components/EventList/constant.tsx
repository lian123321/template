import { EControlEventType } from "../../../../../../../../store/modules/editor";

export const eventKeyMapToLabel = {
  [EControlEventType.Click]: "切换场景",
  [EControlEventType.AudioPlay]: "播放音频",
};
