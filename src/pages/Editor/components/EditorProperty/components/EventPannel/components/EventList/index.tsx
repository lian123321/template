import React from "react";
import { useSelector } from "react-redux";
import {
  EControlEventType,
  IState,
} from "../../../../../../../../store/modules/editor";
import { TRootState } from "../../../../../../../../store/types";
import $style from "./style.module.less";
import { Close } from "@icon-park/react";
import { eventKeyMapToLabel } from "./constant";

interface IProps {
  handleClick: (key: EControlEventType) => void;
  handleCloseClick: (key: EControlEventType) => void;
}

const EventList: React.FC<IProps> = ({ handleClick, handleCloseClick }) => {
  const editor = useSelector<TRootState, IState>((state) => state.editor);

  const { selectControlId } = editor;

  const currentControl = editor.project.template.controls[selectControlId];

  const event = currentControl.event || {};

  return (
    <div>
      {Object.keys(event).map((key, i) => {
        const eventType = key as EControlEventType;

        return (
          <div key={i} className={$style.eventListItemWrapper}>
            <div
              className={$style.labelWrapper}
              onClick={() => handleClick(eventType)}
            >
              {eventKeyMapToLabel[eventType]}
            </div>
            <div
              className={$style.CloseWrapper}
              onClick={() => handleCloseClick(eventType)}
            >
              <Close size="14" theme="filled" fill="blue" />
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default EventList;
