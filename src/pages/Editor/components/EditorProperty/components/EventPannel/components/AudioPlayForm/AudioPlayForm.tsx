import { Form } from "antd";
import _ from "lodash";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  createItem,
  ICreateItemProps,
} from "../../../../../../../../components/Form";
import {
  EControlEventType,
  getAddEventOfCurrentControl,
  IAudioPlayEvent,
  IProject,
  TEvent,
} from "../../../../../../../../store/modules/editor";
import { TRootState } from "../../../../../../../../store/types";
import { IToggleStageEvent } from "../../../../../../../../store/modules/editor";
import { audioPlayConfigList } from "../../constant";
import { MusicUpload } from "./components/MusicUpload/index";
interface IProps {
  initData: TEvent | undefined;
}

export const AudioPlayForm: React.FC<IProps> = ({ initData }) => {
  const dispatch = useDispatch();

  const [form] = Form.useForm<IAudioPlayEvent>();

  useEffect(() => {
    if (!initData) {
      return;
    }
    form.setFieldsValue(initData[EControlEventType.AudioPlay] || {});
  }, [initData]);

  const project = useSelector<TRootState, IProject>(
    (state) => state.editor.project
  );
  const [config, setConfig] = useState<ICreateItemProps[]>(audioPlayConfigList);

  useEffect(() => {
    const newConfig = _.cloneDeep(config);

    setConfig(newConfig);
  }, [project]);

  const handleChangeControlVal = (formModels: IAudioPlayEvent) => {
    dispatch(
      getAddEventOfCurrentControl({ [EControlEventType.AudioPlay]: formModels })
    );
  };
  const handleValueChange = (
    val: string | number,
    formModels: IAudioPlayEvent
  ) => {
    console.log(val);
    handleChangeControlVal(formModels);
  };

  const eventData = { url: "", ...initData?.[EControlEventType.AudioPlay] };
  const handleUploadChange = (url: string) => {
    const data = { ...eventData };
    data.url = url;
    handleChangeControlVal(data);
  };
  return (
    <Form form={form} onValuesChange={handleValueChange}>
      {config.map((item) => createItem(item))}
      <MusicUpload handleChange={handleUploadChange} data={eventData.url} />
    </Form>
  );
};
