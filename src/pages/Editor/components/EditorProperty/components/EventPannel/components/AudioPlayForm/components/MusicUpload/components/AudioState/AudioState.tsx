import { PauseOne, Play } from "@icon-park/react";
import React from "react";
import $style from "./style.module.less";

interface IProps {
  isPlay: boolean;
}

export const AudioState: React.FC<IProps> = ({ isPlay }) => {
  return (
    <div className={$style.playState}>
      {!isPlay ? (
        <Play size="30" theme="filled" fill="#ccc" />
      ) : (
        <PauseOne size="30" theme="filled" fill="#ccc" />
      )}
    </div>
  );
};
