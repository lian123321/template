export const formate = (current: number, total: number) => {
  const currentFormateTime = getFormateMinute(current);

  const totalFormateTime = getFormateMinute(total);
  return `${currentFormateTime}/${totalFormateTime}`;
};

const getFormateMinute = (num: number): string => {
  const millisecond = num * 1000;
  const formateMinute = Math.floor(millisecond / (1000 * 60));
  const formatsecond = Math.floor(
    (millisecond - formateMinute * 1000 * 60) / 60
  );
  const formateMinuteStr = formateMinute.toString().padStart(2, "0");
  const formatSecondStr = formatsecond.toString().padStart(2, "0");
  return `${formateMinuteStr}:${formatSecondStr}`;
};
