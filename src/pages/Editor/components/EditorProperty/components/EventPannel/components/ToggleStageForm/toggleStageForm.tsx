import { Form } from "antd";
import _ from "lodash";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  createItem,
  ICreateItemProps,
} from "../../../../../../../../components/Form";
import {
  EControlEventType,
  getAddEventOfCurrentControl,
  IProject,
  TEvent,
} from "../../../../../../../../store/modules/editor";
import { TRootState } from "../../../../../../../../store/types";
import { IToggleStageEvent } from "../../../../../../../../store/modules/editor";
import { toggleStageConfigList } from "../../constant";
interface IProps {
  initData: TEvent | undefined;
}

export const ToggleStageForm: React.FC<IProps> = ({ initData }) => {
  const dispatch = useDispatch();

  const [form] = Form.useForm<IToggleStageEvent>();

  useEffect(() => {
    if (!initData) {
      return;
    }
    form.setFieldsValue(initData[EControlEventType.Click] || {});
  }, [initData]);

  const project = useSelector<TRootState, IProject>(
    (state) => state.editor.project
  );
  const [config, setConfig] = useState<ICreateItemProps[]>(
    toggleStageConfigList
  );

  useEffect(() => {
    const newConfig = _.cloneDeep(config);
    const options = project.template.stages.map(({ id, name }) => ({
      label: name,
      value: id,
    }));

    newConfig[1].childrenOptions = options;

    setConfig(newConfig);
  }, [project]);

  const handleValueChange = (
    val: string | number,
    formModels: IToggleStageEvent
  ) => {
    dispatch(
      getAddEventOfCurrentControl({ [EControlEventType.Click]: formModels })
    );
  };

  return (
    <Form form={form} onValuesChange={handleValueChange}>
      {config.map((item) => createItem(item))}
    </Form>
  );
};
