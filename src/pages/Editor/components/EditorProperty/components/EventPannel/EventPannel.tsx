import React, { Dispatch, useState } from "react";
import $style from "./style.module.less";
import { AddFour } from "@icon-park/react";
import { Close } from "@icon-park/react";
import { createItem, EFormType } from "../../../../../../components/Form";
import { Form } from "antd";
import { EventDialog } from "./components/EventDialog";
import { eventList } from "./constant";
import { toggleStageConfigList } from "./constant";
import { ToggleStageForm } from "./components/ToggleStageForm/toggleStageForm";
import { useDispatch, useSelector } from "react-redux";
import {
  EControlEventType,
  getAddEventOfCurrentControl,
  getClearEventOfCurrentControl,
  IProject,
  IState,
  TEvent,
} from "../../../../../../store/modules/editor";
import { TRootState } from "../../../../../../store/types";
import EventList from "./components/EventList";
import { eventKeyMapToLabel } from "./components/EventList/constant";
import { AudioPlayForm } from "./components/AudioPlayForm/AudioPlayForm";
import classNames from "classnames";

interface IProps {}

const eventTypeMapToProcessFn = (dispatch: Dispatch<any>) => ({
  [EControlEventType.Click]() {
    dispatch(
      getAddEventOfCurrentControl({
        [EControlEventType.Click]: {
          delay: 0,
          area: "",
        },
      })
    );
  },
  [EControlEventType.AudioPlay]() {
    dispatch(
      getAddEventOfCurrentControl({
        [EControlEventType.AudioPlay]: {
          url: "",
          delay: 0,
        },
      })
    );
  },
});

const eventTypeMapToElementProcessFn = {
  [EControlEventType.Click](data: TEvent | undefined) {
    return <ToggleStageForm initData={data} />;
  },
  [EControlEventType.AudioPlay](data: TEvent | undefined) {
    return <AudioPlayForm initData={data} />;
  },
};

export const EventPannel: React.FC<IProps> = (props) => {
  const dispatch = useDispatch();

  const editor = useSelector<TRootState, IState>((state) => state.editor);

  const { selectControlId, project } = editor;

  const [selectEventType, setSelectEventType] =
    useState<EControlEventType | null>(null);

  const currentControl = project.template.controls[selectControlId];

  const event = currentControl.event;

  const [showEventList, setShowEventList] = useState(false);

  const [showDialog, setShowDialog] = useState(false);

  const toggleShowEventList = () => {
    setShowEventList(!showEventList);
  };

  const toggleEventDialog = (val: EControlEventType) => {
    toggleShowEventList();
    if (event?.[val]) {
      return;
    }
    const processFn = eventTypeMapToProcessFn(dispatch)[val];
    if (processFn) {
      processFn();
    }
    setSelectEventType(val);
    setShowDialog(true);
  };

  const closeEventDialog = () => {
    setShowDialog(false);
    setSelectEventType(null);
  };

  const handleEventListItemClick = (key: EControlEventType) => {
    setSelectEventType(key);
    setShowDialog(true);
  };

  const handleEventListItemClose = (key: EControlEventType) => {
    dispatch(getClearEventOfCurrentControl(key));
  };

  return (
    <div className={$style.eventPannelWrapper}>
      <div>
        <EventList
          handleClick={handleEventListItemClick}
          handleCloseClick={handleEventListItemClose}
        />
      </div>
      <div className={$style.eventPannelOptWrapper}>
        <div
          className={$style.eventPannelOptAddEvent}
          onClick={toggleShowEventList}
        >
          <AddFour size="16" fill="#3955f5" theme="outline" />
          <span className={$style.eventText}>触发事件</span>
        </div>

        {showEventList && (
          <div className={$style.stageListWrapper}>
            {eventList.map((item) => {
              const { label, value } = item;
              return (
                <div
                  className={classNames(
                    $style.stageListItemWrapper,
                    event?.[value] && $style.disable
                  )}
                  key={value}
                  onClick={() => toggleEventDialog(value)}
                >
                  {label}
                </div>
              );
            })}
          </div>
        )}

        {showDialog && (
          <EventDialog
            title={eventKeyMapToLabel?.[selectEventType as EControlEventType]}
            onClose={closeEventDialog}
            content={eventTypeMapToElementProcessFn[
              selectEventType as EControlEventType
            ](event)}
          />
        )}
      </div>
    </div>
  );
};
