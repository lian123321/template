import { Form, Input, InputNumber, Select } from "antd";
import React, { useEffect } from "react";
import {
  EAnimationType,
  EDirection,
} from "../../../../../../components/Render/Button/types";
import { IControl } from "../../../../../../store/modules/editor";
import $style from "./style.module.less";

interface IProps {
  handleChange: (style: IStyleAnimate) => void;
  control: IControl;
}

export interface IStyleAnimate {
  animationName: string;
  animationIterations: number;
  animationDuration: number;
  animationStartValue: number;
  animationEndValue: number;
  animationDirection: string;
}
const FormItem = Form.Item;
const Option = Select.Option;

export const Animation: React.FC<IProps> = ({ handleChange, control }) => {
  const fontOptions = [
    { label: "正常", value: EAnimationType.Normal }, // 时间 初始淡度 最终淡度 次数
    { label: "缩放", value: EAnimationType.Scale }, // 时间 初始 最终 次数
    { label: "旋转", value: EAnimationType.Rotate }, // 时间 初始角度 最终角度 次数
    { label: "淡入", value: EAnimationType.Opacity }, // 时间 初始淡度 最终淡度 次数
  ];

  const directionOptions = [
    { label: "Normal", value: EDirection.Normal },
    { label: "Alternate", value: EDirection.Alternate },
  ];

  const config = {
    [EAnimationType.Rotate]: {
      limit: {
        min: 0,
        max: 360,
      },
      init: {
        min: 0,
        max: 360,
      },
    },
    [EAnimationType.Opacity]: {
      limit: {
        min: 0,
        max: 1,
      },
      init: {
        min: 0,
        max: 1,
      },
    },
    [EAnimationType.Scale]: {
      limit: {
        min: 0,
        max: 20,
      },
      init: {
        min: 1,
        max: 2,
      },
    },
  };
  const [form] = Form.useForm<IStyleAnimate>();

  useEffect(() => {
    form.setFieldsValue(control.style);
  }, [control]);
  const handleFormChange = (value: any, formValue: IStyleAnimate) => {
    const isNormal = formValue.animationName === EAnimationType.Normal;
    if (!isNormal) {
      const animationNameIsChange =
        formValue.animationName !== control.style.animationName;
      const { max, min } = (config as any)[formValue.animationName].init;
      if (animationNameIsChange) {
        formValue.animationStartValue = min;
        formValue.animationEndValue = max;
        formValue.animationDuration = 1000;
      }
    }
    handleChange(formValue);
  };

  const selectVlaue = control.style.animationName as EAnimationType;
  const selectConfig = (config as any)[selectVlaue]?.limit ?? {};
  const isSelectNormal = selectVlaue === EAnimationType.Normal || !selectVlaue;
  return (
    <div>
      <Form form={form} onValuesChange={handleFormChange}>
        <div>
          <div className={$style.labelWrapper}>动画类型</div>

          <FormItem name="animationName" initialValue={EAnimationType.Normal}>
            <Select style={{ width: "100%" }}>
              {fontOptions.map((item) => {
                const { label, value } = item;

                return (
                  <Option key={value} value={value}>
                    {label}
                  </Option>
                );
              })}
            </Select>
          </FormItem>
        </div>

        {isSelectNormal || (
          <>
            <div>
              <div className={$style.labelWrapper}>动画模式</div>

              <FormItem
                name="animationDirection"
                initialValue={EDirection.Normal}
              >
                <Select style={{ width: "100%" }}>
                  {directionOptions.map((item) => {
                    const { label, value } = item;

                    return (
                      <Option key={value} value={value}>
                        {label}
                      </Option>
                    );
                  })}
                </Select>
              </FormItem>
            </div>
            <div className={$style.animationConfigWrapper}>
              <div>
                <div>动画次数</div>
                <FormItem
                  className={$style.labelWrapper}
                  name="animationIterations"
                >
                  <InputNumber defaultValue={1} />
                </FormItem>
              </div>

              <div>
                <div>动画持续时间</div>
                <FormItem
                  className={$style.labelWrapper}
                  name="animationDuration"
                >
                  <InputNumber defaultValue={1000} />
                </FormItem>
              </div>

              <div>
                <div>初始值</div>
                <FormItem
                  className={$style.labelWrapper}
                  name="animationStartValue"
                >
                  <InputNumber max={selectConfig.max} min={selectConfig.min} />
                </FormItem>
              </div>

              <div>
                <div>结束值</div>
                <FormItem
                  className={$style.labelWrapper}
                  name="animationEndValue"
                >
                  <InputNumber max={selectConfig.max} min={selectConfig.min} />
                </FormItem>
              </div>
            </div>
          </>
        )}
      </Form>
    </div>
  );
};
