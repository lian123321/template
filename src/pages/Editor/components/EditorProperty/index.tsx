import { Button, Checkbox, Collapse, Form, Input, Select } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  getChangeActiveControlAction,
  getChangeCurrentStageName,
  IState,
} from "../../../../store/modules/editor";
import { TRootState } from "../../../../store/types";
import $style from "./style.module.less";

// 引入createItem
import { createItem, EFormType } from "../../../../components/Form";
import { Upload } from "../../../../components/Form/components/Upload";
import _ from "lodash";
import { Animation, IStyleAnimate } from "./components/Animation";

const Option = Select.Option;

import TextArea from "antd/lib/input/TextArea";
import { FontPannel, IFontStyle } from "./components/FontPannel";
import { ClickInput } from "../../../../components/ClickInput";
import { MusicUpload } from "../../../../components/Form/components/MusicUpload";
import { EventPannel } from "./components/EventPannel";

interface IProps {}

const Panel = Collapse.Panel;
const EditorProperty: React.FC<IProps> = (props) => {
  const dispatch = useDispatch();

  const editor = useSelector<TRootState, IState>((state) => state.editor);

  const {
    project: { template },
    activeIndex,
    selectControlId,
  } = editor;
  const { stages, controls } = template;

  const currentStage = stages[Number(activeIndex)] || {};

  const haveSelectControl = Number(selectControlId) !== -1;

  const currentControl = controls[Number(selectControlId)] ?? {};
  const box = currentControl.box;
  const pos = box?.pos || [0, 0];
  const size = box?.size || [0, 0];
  const [form] = Form.useForm<{ x: number }>();

  /**
   *
   * 全部一个form 可以
   *
   */

  const [inputValue, setInputValue] = useState("");

  useEffect(() => {
    setInputValue(String(pos[0]));
  }, [pos]);

  const handleStyleChange = (values: IFontStyle | IStyleAnimate) => {
    const newControl = _.cloneDeep(currentControl);
    const style = newControl.style;
    newControl.style = { ...style, ...values };
    dispatch(
      getChangeActiveControlAction({
        id: selectControlId,
        data: { ...newControl },
      })
    );
  };

  const handleChangeStageName = (value: string) => {
    dispatch(getChangeCurrentStageName(value));
  };

  const handleChangeCurrentControl = (value: string) => {
    const newControl = _.cloneDeep(currentControl);
    newControl.name = value;
    dispatch(
      getChangeActiveControlAction({
        id: selectControlId,
        data: { ...newControl },
      })
    );
  };

  const singleFormConfig = [
    {
      type: EFormType.Input,
      componentProps: {
        onChange(e: any) {
          //   getChangeActiveControlAction
          const id = selectControlId;
          const control = controls[id];
          if (control?.box?.pos) {
            control.box.pos[0] = Number(e.target.value);
            dispatch(getChangeActiveControlAction({ id, data: control }));
          }
        },
        prefix: "x:",
        value: pos[0],
      },

      formItemProps: {
        name: "x",
        style: { width: 150 },
        valuePropName: "x",
      },
    },
    {
      type: EFormType.Input,
      componentProps: {
        onChange(e: any) {
          //   getChangeActiveControlAction
          const id = selectControlId;
          const control = controls[id];
          if (control?.box?.pos) {
            control.box.pos[1] = Number(e.target.value);
            dispatch(getChangeActiveControlAction({ id, data: control }));
          }
        },
        prefix: "Y:",
        value: pos[1],
      },

      formItemProps: {
        name: "y",
        style: { width: 150 },
        valuePropName: "y",
      },
    },
    {
      type: EFormType.Input,
      componentProps: {
        onChange(e: any) {
          //   getChangeActiveControlAction
          const id = selectControlId;
          const control = controls[id];
          if (control?.box?.size) {
            control.box.size[0] = Number(e.target.value);
            dispatch(getChangeActiveControlAction({ id, data: control }));
          }
        },
        prefix: "width:",
        value: size[0],
      },

      formItemProps: {
        name: "width",
        style: { width: 150 },
        valuePropName: "width",
      },
    },
    {
      type: EFormType.Input,
      componentProps: {
        onChange(e: any) {
          //   getChangeActiveControlAction
          const id = selectControlId;
          const control = controls[id];
          if (control?.box?.size) {
            control.box.size[1] = Number(e.target.value);
            dispatch(getChangeActiveControlAction({ id, data: control }));
          }
        },
        prefix: "height:",
        value: size[1],
      },

      formItemProps: {
        name: "height",
        style: { width: 150 },
        valuePropName: "height",
      },
    },
  ];

  const pannelListConfig = [
    {
      headerText: "素材名称与内容",
      show: true,
      content: (
        <div className={$style.contentPannelWrapper}>
          {/* <div>场景:{currentStage.name}</div> */}
          <div>
            <div className={$style.labelWrapper}> 场景:</div>
            <ClickInput
              value={currentStage.name}
              handleChange={handleChangeStageName}
            />
          </div>
          {haveSelectControl && (
            <div>
              <div className={$style.labelWrapper}>控件ID:</div>
              <ClickInput
                value={currentControl.name}
                handleChange={handleChangeCurrentControl}
              />
            </div>
          )}
        </div>
      ),
    },
    {
      headerText: "属性",
      show: haveSelectControl && !["audio"].includes(currentControl.type),
      content: (
        <div>
          <Form
            name="basic"
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 16 }}
            initialValues={{ remember: true }}
            onFinish={() => {}}
            onFinishFailed={() => {}}
            autoComplete="off"
            className={$style.positionAndSizeWrapper}
          >
            {singleFormConfig.map((item) => {
              const { componentProps } = item;
              return createItem<typeof componentProps.value>(item);
            })}
          </Form>
        </div>
      ),
    },
    {
      headerText: "资源",
      show:
        haveSelectControl && !["text", "audio"].includes(currentControl.type),
      content: (
        <div>
          <Upload
            value={currentControl.data?.url || ""}
            handleUploadChange={(url: string) => {
              const newControl = _.cloneDeep(currentControl);
              (newControl.data as any).url = url;
              dispatch(
                getChangeActiveControlAction({
                  id: selectControlId,
                  data: { ...newControl },
                })
              );
            }}
          />
        </div>
      ),
    },
    {
      headerText: "资源",
      show: haveSelectControl && ["audio"].includes(currentControl.type),
      content: (
        <div>
          <MusicUpload
            handleUploadChange={(name: string, url: string) => {
              const newControl = _.cloneDeep(currentControl);
              (newControl.data as any).url = url;
              newControl.name = name;
              dispatch(
                getChangeActiveControlAction({
                  id: selectControlId,
                  data: { ...newControl },
                })
              );
            }}
          />
        </div>
      ),
    },
    {
      headerText: "文本内容及样式",
      show: haveSelectControl && ["text"].includes(currentControl.type),
      content: (
        <FontPannel control={currentControl} handleChange={handleStyleChange} />
      ),
    },
    {
      headerText: "动画",
      show: haveSelectControl && !["audio"].includes(currentControl.type),
      content: (
        <Animation control={currentControl} handleChange={handleStyleChange} />
      ),
    },
    {
      headerText: "事件",
      show: haveSelectControl,
      content: <EventPannel />,
    },
  ];
  return (
    <div className={$style.editorPropertyWrapper}>
      <Collapse defaultActiveKey={["1"]} accordion={true}>
        {pannelListConfig
          .filter((item) => item.show)
          .map((item, i) => {
            const { headerText, content } = item;
            return (
              <Panel header={headerText} key={i}>
                {content}
              </Panel>
            );
          })}
      </Collapse>
    </div>
  );
};

export default EditorProperty;
