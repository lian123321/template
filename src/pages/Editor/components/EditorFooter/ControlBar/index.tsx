import React from "react";
import $style from "./style.module.less";

import { useDispatch, useSelector } from "react-redux";
import { Delete, PreviewOpen, PlayOne, Pause } from "@icon-park/react";
import {
  getDelSelectedControlOfId,
  getTogglePlayStateAction,
  IState,
} from "../../../../../store/modules/editor";
import { TRootState } from "../../../../../store/types";

interface IProps {}

const ControlBar: React.FC<IProps> = (props) => {
  const dispatch = useDispatch();

  const editor = useSelector<TRootState, IState>((state) => state.editor);

  const { start: playState } = editor;

  const toolBarList = [
    {
      Icon: Delete,
      onClick() {
        dispatch(getDelSelectedControlOfId());
      },
    },
    {
      Icon: PreviewOpen,
      onClick() {},
    },
    {
      Icon: playState ? Pause : PlayOne,
      onClick() {
        dispatch(getTogglePlayStateAction());
      },
    },
  ];
  return (
    <div className={$style.controlBarWrapper}>
      {toolBarList.map((item, i) => {
        const { Icon, onClick } = item;
        return (
          <div key={i} className={$style.controlBarItem} onClick={onClick}>
            <Icon size="20" fill="#333" theme="outline" />
          </div>
        );
      })}
      <div className={$style.time}>00:00:35</div>
    </div>
  );
};

export default ControlBar;
