import React from "react";
import $style from "./style.module.less";
import { useSelector, useDispatch } from "react-redux";
import { TRootState } from "../../../../../store/types";
import {
  getChangeSelectControlAction,
  IState,
} from "../../../../../store/modules/editor";
import classNames from "classnames";

interface IProps {}

const TimeLine: React.FC<IProps> = (props) => {
  const dispatch = useDispatch();

  const editor = useSelector<TRootState, IState>((state) => state.editor);
  const {
    activeIndex,
    project: { template },
    selectControlId,
  } = editor;
  const selectStageIndex = Number(activeIndex);
  const { stages, controls } = template;
  const activeStage = stages[selectStageIndex];
  const content = activeStage?.content || [];

  const hadnleClick = (id: string) => {
    const action = getChangeSelectControlAction(id);
    dispatch(action);
  };

  return (
    <div className={$style.timeLineWrapper}>
      {content.map((id) => {
        const currentControl = controls[id];
        const isSelected = selectControlId === id;
        return (
          <div
            className={classNames(
              $style.timelineItem,
              isSelected && $style.selectItem
            )}
            key={id}
            onClick={() => hadnleClick(id)}
          >
            <div className={$style.timelineItemControlName}>
              {currentControl.name}
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default TimeLine;
