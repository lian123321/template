import classNames from "classnames";
import React, { useEffect, useRef, useState } from "react";
import { useDispatch } from "react-redux";
import { getChangeCurrentTimeAction } from "../../../../../store/modules/editor";
import $style from "./style.module.less";

interface Props {}

const TimelineColor: React.FC<Props> = (props) => {
  const dispatch = useDispatch();

  const timeBLock = 100;
  const timeBlockArray = new Array(timeBLock).fill(1);

  const timeLineRef = useRef<HTMLDivElement>(null);

  const [offsetX, setOffsetX] = useState<number>(0);

  //   const [isMouseDown, setIsMouseDown] = useState<boolean>(false);

  // 鼠标按下
  const handleMouseDown = (e: MouseEvent) => {
    const x = e.offsetX;
    setOffsetX(x);
    dispatch(getChangeCurrentTimeAction(x / 175));
    // setIsMouseDown(true);
    const timeLineELement = timeLineRef.current;
    if (timeLineELement) {
      // timeLineELement.addEventListener("mousemove", handleMouseMove, false);
    }
  };

  // 鼠标press
  const handleMouseUp = () => {
    const timeLineELement = timeLineRef.current;
    if (timeLineELement) {
      timeLineELement.removeEventListener("mousemove", handleMouseMove, false);
    }
  };
  // 鼠标移动
  const handleProcessMove = (x: number) => {
    setOffsetX(x);
  };
  const handleMouseMove = (e: MouseEvent) => {
    const offsetX = e.offsetX;
    handleProcessMove(offsetX);
    dispatch(getChangeCurrentTimeAction(offsetX / 150));
  };

  useEffect(() => {
    const timeLineELement = timeLineRef.current;
    if (timeLineELement) {
      timeLineELement.addEventListener("mousedown", handleMouseDown, false);
      timeLineELement.addEventListener("mouseup", handleMouseUp, false);
    }
  }, []);

  return (
    <div className={$style.timelineColorWrapper} ref={timeLineRef}>
      {timeBlockArray.map((item, i) => {
        const isSplitLine = i % 10 === 0;
        const isSplitMiddleLine = i % 5 === 0;
        let splitClassName = "";
        let text = "";

        if (isSplitMiddleLine) {
          splitClassName = $style.splitMiddleLine;
        }

        if (isSplitLine) {
          splitClassName = $style.splitLine;
          text = `${i / 10}s`;
        }

        return (
          <div
            key={i}
            className={classNames($style.timelineColorItem, splitClassName)}
          >
            {text}
          </div>
        );
      })}

      <div className={$style.selectLine} style={{ left: `${offsetX}px` }}></div>
    </div>
  );
};

export default TimelineColor;
