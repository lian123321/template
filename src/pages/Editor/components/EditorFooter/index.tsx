import React from "react";
import ControlBar from "./ControlBar";
import $style from "./style.module.less";
import TimeLine from "./TimeLine";
import TimelineColor from "./TimelineColor";

interface IProps {}

const EditorFooter: React.FC<IProps> = (props) => {
  return (
    <div className={$style.editorFooterWrapper}>
      <div className={$style.footerUp}>
        <ControlBar />
        <TimelineColor />
      </div>
      <div className={$style.footerDown}>
        <TimeLine />
      </div>
    </div>
  );
};

export default EditorFooter;
