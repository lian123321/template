import React, { useEffect, useState } from "react";
import { Button, Layout, message } from "antd";
import { Content, Footer, Header } from "antd/lib/layout/layout";
import Sider from "antd/lib/layout/Sider";
import $style from "./style.module.less";
import EditorHeader from "./components/EditorHeader";
import Template from "../../components/Template";
import EditorStages from "./components/EditorStages";
import EditorSide from "./components/EditorSide";
import EditorProperty from "./components/EditorProperty";
import EditorFooter from "./components/EditorFooter";
import { parse } from "query-string";
import { useNavigate } from "react-router-dom";
import { getTemplate } from "../../services/template";
import { getInitProject, IProject } from "../../store/modules/editor";
import { useDispatch } from "react-redux";
import EditorPreview from "./components/EditorPreview/EditorPreview";
import { Loading } from "./components/Loading";
// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface IProps {}

const Editor: React.FC<IProps> = (props) => {
  const history = useNavigate();

  const dispatch = useDispatch();

  const [loading, setLoading] = useState(true);
  useEffect(() => {
    const search = location.search;
    const { id } = parse(search) as { id: string | undefined };

    if (!id) {
      history("/");
      return;
    }
    setLoading(true);
    getTemplate(id)
      .then((res) => {
        const { id, name, template } = res;
        const objectTemplate = JSON.parse(template);
        const newProject: IProject = {
          id: String(id),
          name,
          template: objectTemplate,
        };
        dispatch(getInitProject(newProject));

        setLoading(false);
      })
      .catch((e) => {
        message.error(e);
      });
  }, []);

  return (
    <div className={$style.editorWrapper}>
      {loading && <Loading />}
      <Layout className={$style.layout}>
        <Header>
          <EditorHeader />
        </Header>
        <Layout className={$style.mainLayout}>
          <Sider className={$style.menu}>
            <EditorSide />
          </Sider>
          <Content className={$style.content}>
            <div className={$style.contentWrapper}>
              <div className={$style.paintWrapper}>
                <Template />
              </div>
              <div className={$style.propertyPannel}>
                <EditorProperty />
              </div>
            </div>
            <EditorFooter />
          </Content>
          {/* <Sider className={$style.propertyPannel}>
            <EditorProperty />
          </Sider> */}
        </Layout>
      </Layout>
    </div>
  );
};

export default Editor;
