import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { useGetHomeRoutes } from "../../route/hooks/useGetHomeRoutes";
import { Content } from "./components/Content";
import { Header } from "./components/Header";
import { Home as CHome } from "./components/Home";
import $style from "./style.module.less";
// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface IProps {}

export const Home: React.FC<IProps> = (props) => {
  const routes = useGetHomeRoutes();
  return (
    <div className={$style.homeWrapper}>
      <Header />
      <Routes>
        {routes.map((route) => {
          const { path, key, show, component: Component } = route;
          if (!show) {
            return null;
          }
          return <Route path={path} key={key} element={<Component />} />;
        })}
      </Routes>
    </div>
  );
};
