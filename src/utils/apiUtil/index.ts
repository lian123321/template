export const getUrl = (): string => {
   return isOnline ? 'http://150.158.141.76:8800' : 'http://localhost:8800'
}

export const link = (path: string) => {
   const a = document.createElement('a');
   a.href = path;
   a.target = "_blank";
   a.click();
}