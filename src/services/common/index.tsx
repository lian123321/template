export interface IReturnRquestData<T = unknown> {
  code: string;
  data: T;
}
