import { IProject } from './../../store/modules/editor/types';
import { templateRequest } from "../../api/lib/request"


export interface ICreateTemplateReqData  {
    id: number;
}
export const createTemplate = (): Promise<ICreateTemplateReqData> => {
    return templateRequest.get<ICreateTemplateReqData,null>('/media/template/create')
}

export interface IGetTemplateReqData {
    id: string;
    name: string;
    template: string;
}

export const getTemplate = (id: string): Promise<IGetTemplateReqData> => {
    return templateRequest.get<IGetTemplateReqData>(`/media/template/get_template?id=${id}`);
}


export const saveTemplate = (project: IProject): Promise<null> => {
    return templateRequest.post<null, IProject>('/media/template/save_template',project);
}

interface IGetTemplateListResData {
    pagination: {
        total: number;
        pageNum: number;
    },
    list: IGetTemplateReqData[];
}
export const getTemplateList = (pageSize: number, pageNum: number): Promise<IGetTemplateListResData> => {
    return templateRequest.get<IGetTemplateListResData, { pageSize: number, pageNum: number }>('/media/template/get_template_list', {params: { pageNum, pageSize }});
}