import { useMemo } from "react";
import Editor from "../../../pages/Editor";
import { Content } from "../../../pages/Home/components/Content";
import { Home } from "../../../pages/Home/components/Home";
import { TRoutes } from "../../types";

const homePath = "/";

const myProject = "/project";

export enum ERouteKey {
  homePath = "homePath",
  myProject = "myProject",
}
// 路由跳转的时候必须从这里那路由path
export const routePath = {
  [ERouteKey.homePath]: homePath,
  [ERouteKey.myProject]: myProject,
};

export const useGetHomeRoutes = (): TRoutes => {
  const routes = useMemo((): TRoutes => {
    const routes = [
      {
        title: "首页",
        key: ERouteKey.homePath,
        show: true,
        component: Home,
      },
      {
        title: "我的项目",
        key: ERouteKey.myProject,
        show: true,
        component: Content,
      },
    ];

    return routes.map((item) => ({
      ...item,
      path: (routePath as any)[item.key],
    }));
  }, []);

  return routes;
};
