import { useMemo } from "react";
import Editor from "../../../pages/Editor";
import { Home } from "../../../pages/Home";
import { TRoutes } from "../../types";

const templatePath = "/template";
const homePath = "/*";

export enum ERouteKey {
  templatePath = "templatePath",
  homePath = "homePath",
}
// 路由跳转的时候必须从这里那路由path
export const routePath = {
  [ERouteKey.templatePath]: templatePath,
  [ERouteKey.homePath]: homePath,
};

export const useGetRoutes = (): TRoutes => {
  const routes = useMemo((): TRoutes => {
    const routes = [
      {
        title: "首页",
        key: ERouteKey.homePath,
        show: true,
        component: Home,
      },
      {
        title: "template",
        key: ERouteKey.templatePath,
        show: true,
        component: Editor,
      },
    ];

    return routes.map((item) => ({
      ...item,
      path: (routePath as any)[item.key],
    }));
  }, []);

  return routes;
};
