// 路由数据结构
export interface IRouteItem {
    title: string;
    key: string;
    show: boolean;
    path: string;
    component: React.ComponentType;
}

export type TRoutes = Array<IRouteItem>;